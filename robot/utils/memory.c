#include "memory.h"
#include <stdbool.h>
#include <stdint.h>

#ifdef _USE_RTOS_ALLOCATION

#include "FreeRTOS.h"
#include "portable.h"


void* malloc(size_t size)
{    
    return pvPortMalloc(size);
}


void free(void* ptr)
{
    vPortFree(ptr);
}

#else

#define MEM_SIZE 0x4000

static uint8_t memBuffer[MEM_SIZE];


struct _MemBlockHeader
{
    size_t size;
    struct _MemBlockHeader* next;
};

typedef struct _MemBlockHeader* MemBlockHeader;

typedef struct
{
    MemBlockHeader head;
    MemBlockHeader tail;
} MemBlockList;

static size_t freeMemSize;
static bool isBufferInitialized = false;
static MemBlockList freeList;
static MemBlockList allocList;

extern char _end;  // begining of the _user_heap_stack memory section;

static void initBuffer()
{
    freeList.head = (MemBlockHeader) memBuffer;
    freeList.head->size = MEM_SIZE - sizeof(struct _MemBlockHeader);
    freeMemSize = freeList.head->size;
    freeList.head->next = 0;
    freeList.tail = freeList.head;
    allocList.head = (MemBlockHeader)0;
    allocList.tail = allocList.head;
    isBufferInitialized = true;
}

static inline void appendElemToList(MemBlockList* list, MemBlockHeader elem)
{
    if(!list->tail)
    {
        list->tail = elem;
    }
    else
    {
        list->tail->next = elem;
        list->tail = list->tail->next;
    }

    if(!list->head)
        list->head = list->tail;

    list->tail->next = 0;
}

void* malloc(size_t size)
{
    if(!isBufferInitialized)
        initBuffer();

    const size_t headerSize = sizeof(struct _MemBlockHeader);

    size_t sizeToAlloc = (size % sizeof(size_t)) ?
            (size / sizeof(size_t) * sizeof(size_t) + sizeof(size_t)) :
            size;

    if(freeMemSize < headerSize)
        return 0;

    if(!freeList.head || sizeToAlloc > freeMemSize - headerSize)
    {
        return 0;
    }

    MemBlockHeader currentBlock = freeList.head,
            prevFreeBlock	  = 0;

    size_t allocHeaderSize = 0;

    while(currentBlock)
    {
        bool isSpaceAvailable = false;

        if(currentBlock->size == sizeToAlloc)
        {
            if(prevFreeBlock)
            {
                prevFreeBlock->next = currentBlock->next;
            }
            else
            {
                freeList.head = currentBlock->next;
            }

            if(currentBlock == freeList.tail)
            {
                freeList.tail = prevFreeBlock;
            }

            isSpaceAvailable = true;

        }
        else if(currentBlock->size >= sizeToAlloc + headerSize)
        {
            MemBlockHeader nextFreeBlock = (MemBlockHeader)((uint8_t*) currentBlock + headerSize + sizeToAlloc);
            nextFreeBlock->next = currentBlock->next;
            nextFreeBlock->size = currentBlock->size - headerSize - sizeToAlloc;
            currentBlock->size = sizeToAlloc;
            currentBlock->next = 0;
            allocHeaderSize += headerSize;

            if(prevFreeBlock)
            {
                prevFreeBlock->next = nextFreeBlock;
            }
            else
            {
                freeList.head = nextFreeBlock;
            }

            if(currentBlock == freeList.tail)
                freeList.tail = nextFreeBlock;

            isSpaceAvailable = true;
        }


        if(isSpaceAvailable)
        {
            if(allocList.tail)
            {
                appendElemToList(&allocList, currentBlock);
            }
            else
            {
                allocList.head = allocList.tail = currentBlock;
                allocList.head->next = 0;
            }

            freeMemSize -= (sizeToAlloc + allocHeaderSize);

            return (uint8_t*) currentBlock + headerSize;
        }

        prevFreeBlock = currentBlock;
        currentBlock = currentBlock->next;
    }

    return 0;

}

void free(void* ptr)
{
    if(!isBufferInitialized)
        initBuffer();

    if(!allocList.head)
        return;

    const size_t headerSize = sizeof(struct _MemBlockHeader);
    MemBlockHeader currentBlock = allocList.head,
            prevAllocBlock = 0;
    bool isBlockFound = false;


    while(currentBlock)
    {
        if(ptr == (uint8_t*) currentBlock + headerSize)
        {
            isBlockFound = true;
            break;
        }

        prevAllocBlock = currentBlock;
        currentBlock = currentBlock->next;
    }


    if(isBlockFound)
    {
        if(prevAllocBlock)
        {
            prevAllocBlock->next = currentBlock->next;
        }
        else
        {
            allocList.head = currentBlock->next;
        }

        if(currentBlock == allocList.tail)
            allocList.tail = currentBlock->next;

        if(!freeList.head)
        {
            freeList.head = freeList.tail = currentBlock;
            freeList.head->next = 0;
            freeMemSize += currentBlock->size;
            return;
        }

        MemBlockHeader curFreeBlock  = freeList.head,
                prevFreeBlock = 0;

        size_t freedBlockSize = currentBlock->size;
        size_t freedHeadersSize = 0;
        size_t mergeCnt = 0, prevCnt = 0;

        // merge contiguous blocks

        while(curFreeBlock)
        {
            if((uint8_t*) currentBlock + headerSize + currentBlock->size == (uint8_t*) curFreeBlock)
            {
                currentBlock->size += curFreeBlock->size + headerSize;
                ++mergeCnt;
            }

            if((uint8_t*) curFreeBlock + headerSize + curFreeBlock->size == (uint8_t*) currentBlock)
            {

                curFreeBlock->size += currentBlock->size + headerSize;
                currentBlock = curFreeBlock;
                ++mergeCnt;
            }

            if(mergeCnt > prevCnt)
            {
                prevCnt = mergeCnt;
                freedHeadersSize += headerSize;

                if(prevFreeBlock)
                {
                    prevFreeBlock->next = curFreeBlock->next;
                }
                else
                {
                    freeList.head = curFreeBlock->next;
                }

                if(curFreeBlock == freeList.tail)
                    freeList.tail = prevFreeBlock;

                if(mergeCnt == 2)
                    break;
            }
            else
            {
                prevFreeBlock = curFreeBlock;
            }

            curFreeBlock = curFreeBlock->next;
        }

        freeMemSize += freedBlockSize + freedHeadersSize;

        appendElemToList(&freeList, currentBlock);

    }

}

#endif
