# Balancing Robot

A self-balancing robot remotely controlled by a PC via WiFi.

![picture](docs/robot.jpg)


On this page:


* [Main hardware components](https://bitbucket.org/marcin87/balancing_robot/overview#markdown-header-main-hardware-components)

* [Electrical schematic](https://bitbucket.org/marcin87/balancing_robot/overview#markdown-header-electrical-schematic)

* [Building the project](https://bitbucket.org/marcin87/balancing_robot/overview#markdown-header-building-the-project)

    * [Setup development environment](https://bitbucket.org/marcin87/balancing_robot/overview#markdown-header-setup-development-environment)

    * [Building robot binaries](https://bitbucket.org/marcin87/balancing_robot/overview#markdown-header-building-robot-binaries)

    * [Flashing](https://bitbucket.org/marcin87/balancing_robot/overview#markdown-header-flashing)
    
    * [Building PC side application](https://bitbucket.org/marcin87/balancing_robot/overview#markdown-header-building-pc-side-application)

* [Remote control](https://bitbucket.org/marcin87/balancing_robot/overview#markdown-header-remote-control)

    * [GUI app](https://bitbucket.org/marcin87/balancing_robot/overview#markdown-header-gui-app)
    
    * [Non-GUI app - commands](https://bitbucket.org/marcin87/balancing_robot/overview#markdown-header-non-gui-app-commands)


## Main hardware components

* Tiva C Launchpad (TM4C123G) - master board 

    Controls of the robot basic functions and other HW components.

* Tiva C Launchpad (TM4C123G) - slave board

    Controls the WiFi module and forwarding messages between WiFI module and master board.

* CC3100BOOST - WiFi module

    Establishes connection with WiFI Access Point

* MPU6050 - Gyro/Accelerometer

* 2x MCP23017 - GPIO expanders

    Provides additional IO pins for a motor controller, encoders, leds

* L298N - motor controller

    Controls 2 12V DC motors

* LM2596 - DC voltage regulator (converter)

    Covnerts 12V to 5V suitable to power the robot's ICs

* 2x 22CL-3501PG Namiki DC gear motors with encoders


![picture](docs/level1.jpg "Level 1")

![picture](docs/level2.jpg "Level 2")

![picture](docs/level3.jpg "Level 3")

## Electrical schematic

![picture](docs/electrical_schematics.png)

## Building the project

### Setup development environment

1. Create directory for holding tools:

        mkdir ~/Embedded

2. Install dependencies

        sudo dpkg –add-architecture i386
        sudo apt-get update
        sudo apt-get install flex bison libgmp3-dev libmpfr-dev libncurses5-dev libmpc-dev autoconf texinfo build-essential libftdi-dev python-yaml zlib1g-dev libtool
        sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386	     
        sudo apt-get install libusb-1.0-0 libusb-1.0-0-dev

3. Download GNU ARM toolchain for Linux from [Launchpad](https://launchpad.net/gcc-arm-embedded/+download) and extract it to *~/Embedded/*

        tar -xvf gcc-arm-none-eabi-4_8-2014q2-20140609-linux.tar.bz2 -C ~/Embedded


4. Add the extracted directory's *bin* folder to the *PATH* variable

        export PATH=$PATH:$HOME/Embedded/gcc-arm-none-eabi-4_8-2014q1/bin

5. Download *lm4flash* utility and compile it

        cd ~/Embedded
        git clone git://github.com/utzig/lm4tools.git
        cd lm4tools/lm4flash/
        make

6. Clone the Balancing Robot project

        cd ~/Embedded
        git clone https://bitbucket.org/marcin87/balancing_robot.git


### Building robot binaries
#### Master board
```bash
cd ~/Embedded/balancing_robot/robot
make clean
make master
```
#### Slave board
```bash
cd ~/Embedded/balancing_robot/robot
make clean
make slave
```
### Flashing
Connect the Tiva Launchpad board for which the binary was built (master or slave) via USB. Then type the following command:
```bash
cd ~/Embedded/lm4tools/lm4flash/
./lm4flash ~/Embedded/balancing_robot/robot/gcc/robot_main.bin
```

### Building PC side application
#### Non-GUI app building
The *readline* library is required by the app. It can be installed using the following command:
```bash
sudo apt-get install libreadline6 libreadline6-dev
```
Building the Non-GUI application:
```bash
cd ~/Embedded/balancing_robot/robot/linux_pc
make
```
The built app is placed in *~/Embedded/balancing_robot/robot/linux_pc/out* directory

#### GUI app building
For building the GUI application QT framework is required.

The QT robot project can found in *~/Embedded/balancing_robot/robot/linux_pc/robot_gui* directory


## Remote control

### GUI app
Current implementation of the application allow only for establish connection with the robot, collecting logs and adjusting PID controller parameters.

![picture](docs/robot_gui.jpg)


### Non-GUI app - commands

```
getLogs <master/slave>
```
Gets runtime logs from either master or slave board.

```
getFreeHeap <master/slave>
```
Gets amount of available heap memory on either master or slave board.

```
getTaskList <master/slave>
```
Gets the list of currently running tasks (processes) on either of the boards and presents the information about them (task name, task id, task priority, left stack size).

```
setWheelSpeed <wheelId> <speed>
```
Sets the speed of a given wheel.

```
wheelRun <wheelId> <direction> <rotations>
```
Makes a wheel rotate in a given direction a given numer of revolutions.

```
setTaskPriority <master/slave> <taskId> <priority>
```
Sets a priority of task with a given priority.

```
getPostmortem <master/slave>
```
Collects postmortem logs from previous runtime stored in persistent memory after system crash.

```
update <master/slave> [options]
```
Builds a binary for a master or slave board end perform wireless SW update of the robot.     
Options:
--single_word, -sw : sends data word by word instead of 32-word packets     
--force_newest, -fn   : forces change active partition to the one with newest sw version

```
getFuncName <master/slave> <partition> <address>
```
Decodes function name given an address from stacktrace and partition number (1 or 2).

```
readMpuReg <regAddress>
```
Reads a register of accel/gyro board.

```
writeMpuReg <regAddress> <value>
```
Writes a value to a accel/gyro board's register.

```
getMpuData [options]
```
Gets accelerometer and gyroscope measuremnts.      
Options:      
--continuous, -c   : receives data continuously     
--periodic, -p <period>  : receives data periodically, period in milliseconds must be specified     
--dump, -d <filename>  : dumps data to file

```
setPidDirection <direction>
```
Set PID controller direction (0 Direct, 1 - Reverse).

```
setMctrlPeriod <period>
```
Sets motion control loop period - rate at which accel/gyro measurements are read, PID controller is updated and output applied to robot's wheels.

```
getMctrlData <dataType> [options]
```
Gets data produced by motion control SW component (dataType: 0 - inclination, 1 - pid output).      
Options:      
--continuous, -c   : receives data continuously     
--periodic, -p <period>  : receives data periodically, period in milliseconds must be specified     
--dump, -d <filename>  : dumps data to file

