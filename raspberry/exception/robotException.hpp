#pragma once

#include <stdexcept>
#include <string>

namespace exception {

struct RobotException : public std::runtime_error
{
    RobotException(const std::string& message) : std::runtime_error(message) { }   
    
};

}
