#pragma once

#include <vector>
#include <chrono>

namespace spicom {

class Spidev
{
public:
    enum class Mode
    {
	CPHA,
	CPOL,
	LSB_FIRST,
	CS_HIGH,
	THREE_WIRE,
	NO_CS,
	READY
    };

    using SpeedHz = unsigned;
    Spidev(SpeedHz speed,
	    const std::vector<Mode>& modes,
	    unsigned bitsPerWord = 8,
	    std::chrono::microseconds delay = std::chrono::microseconds(0));
    ~Spidev();

    std::vector<char> transfer(const std::vector<char>& txData);
private:
    void setMode(const std::vector<Mode>& modes);
    void setBitsPerWord(char bitsPerWord);
    void setSpeed(SpeedHz speed);
    int _fd;
    SpeedHz _speed;
    char _bitsPerWord;
    std::chrono::microseconds _delay;
};


}
