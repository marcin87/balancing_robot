#include "spicom.hpp"
#include "config/configAccess.hpp"
#include "exception/robotException.hpp"
#include "service/addressFinder.hpp"
#include "serdes/spiDeserializer.hpp"
#include "message/address.hpp"
#include <algorithm>
#include <iterator>
#include <sstream>
#include <iostream>
#include <unordered_map>

namespace spicom{

namespace {


class SpicomException : public exception::RobotException
{
public:
    SpicomException(const std::string& message) :
	RobotException("[Spicom] " + message)
    {
    
    }

};

constexpr char START_FRAME_ID = 0xF1,
	       DUMMY_FRAME_ID = 0xE1,
	       DATA_FRAME_ID  = 0xE2;

constexpr unsigned DUMMY_FRAME_LEN = 6;

enum class StateId
{
    NONE,
    IDLE_STATE,
    START_FRAME_STATE,
    DUMMY_FRAME_STATE,
    DATA_FRAME_LEN_STATE,
    DATA_FRAME_PAYLOAD_STATE,
    ERROR_STATE
};

const char* stateIdToStr(StateId stateId)
{
    switch(stateId)
    {
    case StateId::NONE:
	return "NONE";
    case StateId::IDLE_STATE:
	return "IDLE_STATE";
    case StateId::START_FRAME_STATE:
	return "START_FRAME_STATE";
    case StateId::DUMMY_FRAME_STATE:
	return "DUMMY_FRAME_STATE";
    case StateId::DATA_FRAME_LEN_STATE:
	return "DATA_FRAME_LEN_STATE";
    case StateId::DATA_FRAME_PAYLOAD_STATE:
	return "DATA_FRAME_PAYLOAD_STATE";
    case StateId::ERROR_STATE:
	return "ERROR_STATE";
    }

    return "UNKNOWN";
}

class State
{
public:
    State(StateId id) : _id(id) { }
    virtual ~State() = default;
    StateId getId() {return _id; }
    virtual StateId work(char byte) = 0;
protected:
    StateId _id;
};

class IdleState : public State
{
public:
    IdleState() : State(StateId::IDLE_STATE) { }

    virtual StateId work(char byte) override {
	if(byte == START_FRAME_ID)
	    return StateId::START_FRAME_STATE;

	return StateId::IDLE_STATE;
    }
};

class StartFrameState : public State
{
public:
    StartFrameState() : State(StateId::START_FRAME_STATE) { }

    virtual StateId work(char byte) override {
	switch(byte)
	{
	case DUMMY_FRAME_ID:
	    return StateId::DUMMY_FRAME_STATE;
	case DATA_FRAME_ID:
	    return StateId::DATA_FRAME_LEN_STATE;
	default:
	    break;
	}

	return StateId::ERROR_STATE;
    }

};

class DummyFrameState : public State
{
public:
    DummyFrameState() :
	State(StateId::DUMMY_FRAME_STATE),
	_count(0)
    {
    }

    virtual StateId work(char byte) override {
	++_count;
	
	if(_count >= DUMMY_FRAME_LEN)
	{
	    _count = 0;
	    return StateId::IDLE_STATE;
	}

	return StateId::DUMMY_FRAME_STATE;
    }
   
private:
     unsigned _count;
};

class DataFrameLenState : public State
{
public:
    DataFrameLenState(unsigned& length, std::vector<char>& rxBuffer, bool& isFrameReady) :
	State(StateId::DATA_FRAME_LEN_STATE),
	_length(length),
	_rxBuffer(rxBuffer),
	_isFrameReady(isFrameReady)
    {
    }

    virtual StateId work(char byte) override {
	if(_isFrameReady)
	    return StateId::NONE;

	_length = byte;
	_rxBuffer.resize(_length);
	
	if(!_length)
	    return StateId::IDLE_STATE;

	return StateId::DATA_FRAME_PAYLOAD_STATE;
    }

private:
    unsigned& _length;
    std::vector<char>& _rxBuffer;
    bool& _isFrameReady;
};

class DataFramePayloadState : public State
{
public:
    DataFramePayloadState(const unsigned& length, std::vector<char>& rxBuffer, bool& isFrameReady) :
	State(StateId::DATA_FRAME_PAYLOAD_STATE),
	_length(length),
	_rxBuffer(rxBuffer),
	_isFrameReady(isFrameReady),
	_count(0)
    {
    }

    virtual StateId work(char byte) override {
	if(!_length)
	    return StateId::ERROR_STATE;

	_rxBuffer.at(_count++) = byte;
	
	if(_count >= _length)
	{
	    _count = 0;
	    _isFrameReady = true;
	    return StateId::IDLE_STATE;
	}

	return StateId::DATA_FRAME_PAYLOAD_STATE;
    }
private:
    const unsigned& _length;
    std::vector<char>& _rxBuffer;
    bool& _isFrameReady;
    unsigned _count;
};

class ErrorState : public State
{
public:
    ErrorState(log::Logger& log, unsigned& errorCount) :
	State(StateId::ERROR_STATE),
	_log(log),
	_errorCount(errorCount)
    {
    }
    
    virtual StateId work(char byte) override {
	_log.error("RX frame error occured");
	++_errorCount;

	if(byte == START_FRAME_ID)
	    return StateId::START_FRAME_STATE;

	return StateId::IDLE_STATE;
    }
private:
    log::Logger& _log;
    unsigned& _errorCount;
};


inline auto convertSpiMode(config::SpiMode mode)
{
    switch(mode)
    {
    case config::SpiMode_CPHA:
	return Spidev::Mode::CPHA;
    case config::SpiMode_CPOL:
	return Spidev::Mode::CPOL;
    case config::SpiMode_LSB_FIRST:
	return Spidev::Mode::LSB_FIRST;
    case config::SpiMode_CS_HIGH:
	return Spidev::Mode::CS_HIGH;
    case config::SpiMode_THREE_WIRE:
	return Spidev::Mode::THREE_WIRE;
    case config::SpiMode_NO_CS:
	return Spidev::Mode::NO_CS;
    case config::SpiMode_READY:
	return Spidev::Mode::READY;
    default:
	break;
    }
    
    throw SpicomException("Can't convert unrecognized spi mode");
}

inline auto readModesFromConf()
{
    auto conf = config::ConfigAccess().get();
    auto modesVec =  conf->spicom()->spiModes();
    std::vector<Spidev::Mode> result;

    std::for_each(modesVec->begin(), modesVec->end(), [&](const auto& mode){
	    result.push_back(convertSpiMode(static_cast<config::SpiMode>(mode)));
    });

    return std::move(result);
}

inline unsigned readSpeedFromConf()
{
    auto conf = config::ConfigAccess().get();
    return conf->spicom()->speed();
}

inline unsigned readBitsPerWordFromConf()
{
    auto conf = config::ConfigAccess().get();
    return conf->spicom()->bitsPerWord();
}

inline auto readDelayFromConf()
{
    auto conf = config::ConfigAccess().get();
    return std::chrono::microseconds(conf->spicom()->delay());
}

void printBytes(log::Logger& logger, const std::vector<char>& bytes)
{
    std::ostringstream byteStream;
    byteStream << std::hex;
    std::string byteString;
    const std::string separator(", 0x");

    if(!bytes.empty())
    {
	byteString = "0x";
	std::copy(bytes.cbegin(),
		  bytes.cend(),
		  std::ostream_iterator<unsigned>(byteStream, separator.c_str()));
    }
    
    byteString += byteStream.str();
    auto lastSep = byteString.rfind(separator);

    if(lastSep != std::string::npos)
	byteString.erase(lastSep, byteString.length());

    logger.debug("Bytes: %s", byteString.c_str());    

}

} // anonymous namespace

class StateMachine
{
public:
    StateMachine(std::vector<std::unique_ptr<State>>&& states, StateId initialState, log::Logger& logger) :
	_currentState(initialState),
	_log(logger)
    { 
	std::for_each(std::make_move_iterator(states.begin()),
		      std::make_move_iterator(states.end()),
		      [&](auto&& state) { _states.emplace(state->getId(), std::move(state)); });
    }

    ~StateMachine() = default;

    bool update(char byte) {
//	_log.info("SM byte: %d, state=%s", byte, stateIdToStr(_currentState));
	auto state = _states.at(_currentState)->work(byte);

	if(state == StateId::NONE)
	    return false;

	_currentState = state;
	
	return true;
    }
private:
    using IdToStateMap = std::unordered_map<StateId, std::unique_ptr<State>>;

    IdToStateMap _states;
    StateId _currentState;
    log::Logger& _log;
};


Spicom::Spicom(const std::string& name, service::ServiceId serviceId, std::chrono::milliseconds msgWaitTime) :
    service::Service(name, serviceId, msgWaitTime),
    _spidev(readSpeedFromConf(),
	    readModesFromConf(),
	    readBitsPerWordFromConf(),
	    readDelayFromConf()),
    _log(name),
    _msgSender(getId()),
    _rxBufferLength(0),
    _rxErrorCount(0),
    _isRxMsgReady(false)
{
    std::vector<std::unique_ptr<State>> states;
    states.push_back(std::make_unique<IdleState>());
    states.push_back(std::make_unique<StartFrameState>());
    states.push_back(std::make_unique<DummyFrameState>());
    states.push_back(std::make_unique<DataFrameLenState>(_rxBufferLength, _rxMsgBuffer, _isRxMsgReady));
    states.push_back(std::make_unique<DataFramePayloadState>(_rxBufferLength, _rxMsgBuffer, _isRxMsgReady));
    states.push_back(std::make_unique<ErrorState>(_log, _rxErrorCount));

    _stateMachine = std::make_unique<StateMachine>(std::move(states), StateId::IDLE_STATE, _log);
}

Spicom::~Spicom() = default;


void Spicom::doRun()
{
    static std::vector<char> dummyFrame{START_FRAME_ID, DUMMY_FRAME_ID, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0};
    
    auto rxData = _spidev.transfer(dummyFrame);

    handleRxData(rxData);
}

void Spicom::doStart()
{
    _log.info("Starting spicom");
}

void Spicom::doStop()
{
    _log.info("Stopping spicom");

}

void Spicom::handleUnregisteredMsg(std::unique_ptr<message::Msg> msg)
{
    _log.info("Sending message id=%u to spi", msg->getMsgId());
    const unsigned SLOT_FIELD_INDEX = 6;
    const unsigned SPI_FRAME_HEADER_SIZE = 3;
    const auto payloadSize = msg->getPayloadSize();
    std::vector<char> msgFrame;
    msgFrame.reserve(payloadSize + SPI_FRAME_HEADER_SIZE);
    msgFrame.push_back(START_FRAME_ID);
    msgFrame.push_back(DATA_FRAME_ID);
    msgFrame.push_back(static_cast<char>(payloadSize));
    std::copy(msg->getPayloadRawBuffer(),
	      msg->getPayloadRawBuffer() + payloadSize,
	      std::back_inserter(msgFrame));
    msgFrame.at(SLOT_FIELD_INDEX) = msg->getSender();
    printBytes(_log, msgFrame); 
    auto rxData = _spidev.transfer(msgFrame);
    handleRxData(rxData);
}

void Spicom::handleRxData(const std::vector<char>& data)
{
    //_log.debug("Handle RX data ...");
    //printBytes(_log, data);
    const unsigned MAX_RETRIES = 50;
    unsigned retryCount = 0;
    auto dataIt = data.begin();
    
    while(dataIt != data.end())
    {
	if(!_stateMachine->update(*dataIt))
	{
	    if(retryCount++ >= MAX_RETRIES)
		throw SpicomException("Max retries count reached when handling rx data");

	    if(_isRxMsgReady)
		handleReadyMsg();
	}
	else
	{
	    ++dataIt;
	}
    }

    if(_isRxMsgReady)
	handleReadyMsg();
}

void Spicom::handleReadyMsg()
{
    _log.debug("Handle ready msg ...");
    printBytes(_log, _rxMsgBuffer);
    _isRxMsgReady = false;

    serialization::SpiDeserializer deserializer(_rxMsgBuffer);
    auto msg = deserializer.deserialize();
    message::Address address{msg->getPlaceholder()};
    _msgSender.send(std::move(msg), address);
}


}
