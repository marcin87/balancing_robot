#include "spidev.hpp"
#include <unistd.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <errno.h>
#include <string.h>
#include "robotException.hpp"

namespace {

class SpidevException : public exception::RobotException
{
public:
    SpidevException(const std::string& message) :
	RobotException("[Spidev] " + message)
    {
    
    }

};

const char* DEVICE = "/dev/spidev0.0";

char modesToFlags(const std::vector<spicom::Spidev::Mode>& modes)
{
    char flags = 0;

    for(auto mode : modes)
    {
	switch(mode)
	{
	    case spicom::Spidev::Mode::CPHA:
	    flags |= SPI_CPHA;
	    break;
	case spicom::Spidev::Mode::CPOL:
	    flags |= SPI_CPOL;
	    break;
	case spicom::Spidev::Mode::LSB_FIRST:
	    flags |= SPI_LSB_FIRST;
	    break;
	case spicom::Spidev::Mode::CS_HIGH:
	    flags |= SPI_CS_HIGH;
	    break;
	case spicom::Spidev::Mode::THREE_WIRE:
	    flags |= SPI_3WIRE;
	    break;
	case spicom::Spidev::Mode::NO_CS:
	    flags |= SPI_NO_CS;
	    break;
	case spicom::Spidev::Mode::READY:
	    flags |= SPI_READY;
	    break;
	default:
	    break;
	}
	
    }
    
    return flags;
}

}


namespace spicom {

Spidev::Spidev(SpeedHz speed,
	       const std::vector<Mode>& modes,
	       unsigned bitsPerWord,
	       std::chrono::microseconds delay) :
    _fd(-1),
    _speed(speed),
    _bitsPerWord(static_cast<char>(bitsPerWord)),
    _delay(delay)
{
    _fd = ::open(DEVICE, O_RDWR);

    if(_fd < 0)
	throw SpidevException("Could not open device " + std::string(DEVICE));

    setMode(modes);
    setBitsPerWord(bitsPerWord);
    setSpeed(speed);
}

Spidev::~Spidev()
{
    ::close(_fd);
}

std::vector<char> Spidev::transfer(const std::vector<char>& txData)
{
    std::vector<char> rxBuffer(txData.size(), 0);

    spi_ioc_transfer tr = {
	.tx_buf = reinterpret_cast<unsigned long>(txData.data()),
	.rx_buf = reinterpret_cast<unsigned long>(rxBuffer.data()),
	.len = txData.size(),
	.speed_hz = _speed,
	.delay_usecs = _delay.count(),
	.bits_per_word = _bitsPerWord,
	//.cs_change = true
    };

    auto result = ::ioctl(_fd, SPI_IOC_MESSAGE(1), &tr);

    if(result < 1)
	throw SpidevException("Can't send spi message. Error: \"" +  std::string(strerror(errno)) + "\"");

    return rxBuffer;
}

void Spidev::setMode(const std::vector<Mode>& modes)
{
    auto modeFlags = modesToFlags(modes);

    auto result = ::ioctl(_fd, SPI_IOC_WR_MODE, &modeFlags);
    
    if(result < 0)
	throw SpidevException("Can't set spi mode. Error: \"" + std::string(strerror(errno)) + "\"");
}

void Spidev::setBitsPerWord(char bitsPerWord)
{
    auto result = ::ioctl(_fd, SPI_IOC_WR_BITS_PER_WORD, &bitsPerWord);

    if(result < 0)
	throw SpidevException("Can't set bits per word. Error: \"" + std::string(strerror(errno)) + "\"");
}

void Spidev::setSpeed(SpeedHz speed)
{
    auto result = ::ioctl(_fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);

    if(result < 0)
	throw SpidevException("Can't set speed. Error: \"" + std::string(strerror(errno)) + "\"");
}

}
