#pragma once

#include "service/service.hpp"
#include "logger/logger.hpp"
#include "utils/fdPoller.hpp"
#include "message/msgSender.hpp"
#include "spidev.hpp"
#include <string>
#include <memory>
#include <vector>

namespace spicom {

class StateMachine;

class Spicom : public service::Service
{
public:
    Spicom(const std::string& name,
	   service::ServiceId serviceId,
	   std::chrono::milliseconds msgWaitTime = std::chrono::milliseconds(0));
    ~Spicom();
protected:

    virtual void doRun() override;
    virtual void doStart() override;
    virtual void doStop() override;
    virtual void handleUnregisteredMsg(std::unique_ptr<message::Msg> msg);

    void handleRxData(const std::vector<char>& data);
    void handleReadyMsg();

    Spidev _spidev;
    log::Logger _log;
    message::MsgSender _msgSender;
    std::vector<char> _rxMsgBuffer;
    unsigned _rxBufferLength;
    unsigned _rxErrorCount;
    bool _isRxMsgReady;
    std::unique_ptr<StateMachine> _stateMachine;

};



}
