#include "server.hpp"
#include "config/configAccess.hpp"
#include "exception/robotException.hpp"
#include "utils/fdReader.hpp"
#include "utils/fdPoller.hpp"
#include "utils/socketReader.hpp"
#include "serdes/deserializer.hpp"
#include "service/addressFinder.hpp"
#include "messages.h"
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <algorithm>

using FdPoller = utils::FdPoller;

namespace server{

namespace {

constexpr int SOCKET_WAIT_TIME_SEC = 1;

class ServerException : public exception::RobotException
{
public:
    ServerException(const std::string& message) :
	RobotException("[Server] " + message)
    {
    
    }

};

inline unsigned readPortFromConf()
{
    auto conf = config::ConfigAccess().get();
    return conf->server()->port();
}

inline unsigned readMaxConnectionsFromConf()
{
    auto conf = config::ConfigAccess().get();
    return conf->server()->max_connections();
}

auto translateBufferToMsg(std::vector<char>& buffer)
{
    serialization::Deserializer deserializer(reinterpret_cast<unsigned char*>(buffer.data()));
    return deserializer.deserialize();
}

message::Address getServiceAddress(std::string serviceName)
{
    message::Address address;

    if(!service::findServiceAddress(serviceName, address))
	throw ServerException("Could not find \"" + serviceName + "\" address");

    return address;
}

} // anonymous namespace


Server::Server(const std::string& name, service::ServiceId serviceId, std::chrono::milliseconds msgWaitTime) :
    service::Service(name, serviceId, msgWaitTime),
    _port(static_cast<int>(readPortFromConf())),
    _maxConnections(static_cast<int>(readMaxConnectionsFromConf())),
    _log(name),
    _serverSockFd(-1),
    _connectionId(0),
    _fdPoller(FdPoller::PollType::READ, std::chrono::seconds(SOCKET_WAIT_TIME_SEC)),
    _msgSender(getId())
{

}

void Server::doRun()
{
    if(_fdPoller.poll())
    {
	handleFailedSockets(_fdPoller.getFailedFds());
	handleReadySockets(_fdPoller.getReadyFds());
    }
    else
    {
	removeStaleConnections();
    }
}

void Server::doStart()
{
    _log.info("Starting server");

    sockaddr_in serverAddr {};
    
    _serverSockFd = socket(AF_INET, SOCK_STREAM, 0);
   
    if (_serverSockFd < 0) 
	throw ServerException("Failed to open a socket");

    _log.debug("Opened socket");

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_port = htons(_port);

    if (bind(_serverSockFd, (sockaddr*) &serverAddr, sizeof(serverAddr)) < 0) 
    {
	close(_serverSockFd);
	throw ServerException("Failed to bind");
    }

    _log.debug("Started listening for connections on port %d", _port);

    listen(_serverSockFd, _maxConnections);
    _fdPoller.addFd(_serverSockFd);

    sendServerStartedNotif();
}

void Server::doStop()
{
    _log.info("Stopping server");
    close(_serverSockFd);
    auto connNum = _connections.size();

    for(auto& connection : _connections)
	connection.second->stop();
    
    removeStaleConnections();
    _log.info("Stopped all of %d connections", connNum);
}


void Server::createConnection(int sockFd)
{
    using namespace std::chrono_literals;
    _log.info("Creating a new connection from sockFd=%d", sockFd);
    
    const std::string connectionName("connection" + std::to_string(_connectionId));
    auto newConnection = std::make_unique<connection::Connection>(connectionName,
						      _connectionId,
						      1ms,
						      sockFd);
    newConnection->start();
    _log.debug("New connection \"%s\" started", connectionName.c_str());

    _fdPoller.addFd(sockFd);
    _fdToConnIdMap.emplace(sockFd, _connectionId);
    _connIdToFdMap.emplace(_connectionId, sockFd);
    message::Address address;
    
    if(!service::findServiceAddress(connectionName, address))
	throw ServerException("Could not find address for connection \"" + connectionName + "\"");

    sendConnectionStatusNotif(_connectionId, true);
    _connIdToAddress.emplace(_connectionId, address);
    _connections.emplace(_connectionId++, std::move(newConnection));
}

void Server::removeConnection(service::ServiceId connId)
{
    auto connection = _connections.find(connId);
    
    if(connection == _connections.end())
    {
	_log.warning("Could not remove a connection. Connection id=%d does not exist", connId);
	return;
    }

    _log.info("Removing connection \"%s\"", connection->second->getName().c_str());

    if(connection->second->isRunning())
	connection->second->stop();

    auto connFd = _connIdToFdMap.at(connId);
    ::close(connFd);
    _fdPoller.removeFd(connFd);
    _fdToConnIdMap.erase(connFd);
    _connIdToFdMap.erase(connId);
    _connections.erase(connId);
    _connIdToAddress.erase(connId);
    sendConnectionStatusNotif(connId, false);

    _log.debug("Connection removed");
}

void Server::acceptNewConnection()
{
    _log.debug("Accepting a new connection");
    sockaddr_in clientAddr {};
    socklen_t clientLen = sizeof(clientAddr);

    int newSockFd = accept(_serverSockFd, (sockaddr *) &clientAddr, &clientLen);

    if(newSockFd < 0)
    {
	_log.warning("Failed accepting new socket");
	return;
    }

    createConnection(newSockFd);
}

void Server::removeStaleConnections()
{
    auto connNumBefore = _connections.size();
    std::vector<decltype(_connections)::key_type> toBeRemoved;
    std::for_each(_connections.begin(), _connections.end(),
	    [&](const auto& connection) { 
		if(!connection.second->isRunning()) toBeRemoved.emplace_back(connection.first); 
	    });

    auto removedNum = toBeRemoved.size();
    
    for(auto&& connId : toBeRemoved)
	removeConnection(connId);

    if(removedNum)
	_log.debug("Removed %d stale connections", removedNum);
}

void Server::handleFailedSockets(const FdPoller::FileDescriptors& failedFds)
{
    for(const auto& fd : failedFds)
    {
	auto faults = _fdPoller.getFdFaults(fd);
	
	if(faults.empty())
	    continue;

	std::string faultsStr;
	const std::string separator(", ");
	std::for_each(faults.cbegin(), faults.cend(),
		[&](const auto& fault){ faultsStr += fdFaultToStr(fault) + separator; });

	auto lastSep = faultsStr.rfind(separator);

	if(lastSep != std::string::npos)
	    faultsStr.erase(lastSep, faultsStr.length());
	
	auto connId = _fdToConnIdMap.at(fd);
	const auto connName(_connections.at(connId)->getName());
	
	_log.error("Faults %s has been raised for socket %d. Closing connection \"%s\"",
		faultsStr.c_str(), fd, connName);

	removeConnection(connId);
    }
}


void Server::handleReadySockets(const FdPoller::FileDescriptors& readyFds)
{
    for(const auto& fd : readyFds)
    {
	if(fd == _serverSockFd)
	{
	    acceptNewConnection();
	}
	else
	{
	    handleSocketMessage(fd);	     
	}
    }
}

void Server::handleSocketMessage(int sockFd)
{
    try
    {
	auto msgBuffer(std::move(utils::readSocketMsgBuffer(sockFd)));

	if(msgBuffer.empty())
	{
	    _log.info("Client socket closed. Removing connection.");
	    auto connId = _fdToConnIdMap.at(sockFd);
	    removeConnection(connId);
	    return;
	}

	auto msg(translateBufferToMsg(msgBuffer));
	dispatchMessage(std::move(msg), sockFd);
    }
    catch(const exception::RobotException& exception)
    {
	_log.error("Exception: \"%s\"", exception.what());
    }
}

void Server::dispatchMessage(std::unique_ptr<message::Msg> msg, int sockFd)
{
    auto fdToConnId = _fdToConnIdMap.find(sockFd);
    
    if(fdToConnId == _fdToConnIdMap.end())
    {
	_log.warning("Could not find connection service associated with fd=%d." 
		" Discarding the message", sockFd);

	return;
    }

    auto connId = fdToConnId->second;
    
    try
    {
	_msgSender.forward(std::move(msg),  _connIdToAddress.at(connId));
    }
    catch(const exception::RobotException& exception)
    {
	_log.error("Could not forward the message (id=%d) to \"%s\". Exception: \"%s\"",
		msg->getMsgId(), _connections.at(connId)->getName().c_str(), exception.what());
    }
}

void Server::sendServerStartedNotif()
{
    _log.info("Server started - sending notification to the board");

    message::DataMsg<ServerStartedNotifMsgReq> msg(INIT_SERVER_STARTED_NOTIF_MSG_REQ);
    _msgSender.send(std::move(msg), getServiceAddress("spicom"));
}

void Server::sendConnectionStatusNotif(service::ServiceId connId, bool state)
{
    _log.info("Connection id=%d status changed (%s) - sending notification to the board", connId, 
	    state ? "connected" : "disconnected");

    message::DataMsg<ConnectionStatusMsgReq> msg(INIT_CONNECTION_STATUS_MSG_REQ);
    msg.getPayload().state = state;
    _msgSender.send(std::move(msg), getServiceAddress("spicom"));
}


}
