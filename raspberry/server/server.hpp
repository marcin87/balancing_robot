#pragma once

#include "service/service.hpp"
#include "connection/connection.hpp"
#include "utils/fdPoller.hpp"
#include <string>
#include "logger/logger.hpp"
#include "message/address.hpp"
#include "message/msgSender.hpp"
#include <list>
#include <memory>
#include <unordered_map>

namespace utils {
    class FdPoller;
} 

namespace connection {
class Connection;
}

namespace server {


class Server : public service::Service
{
public:
    Server(const std::string& name,
	   service::ServiceId serviceId,
	   std::chrono::milliseconds msgWaitTime = std::chrono::milliseconds(0));

protected:
    using IdToConnectionMap = std::unordered_map<service::ServiceId, std::unique_ptr<connection::Connection>>;
    using FdToConnIdMap = std::unordered_map<int, service::ServiceId>;
    using ConnIdToFdMap = std::unordered_map<service::ServiceId, int>;
    using ConnIdToAddress = std::unordered_map<service::ServiceId, message::Address>;

    virtual void doRun() override;
    virtual void doStart() override;
    virtual void doStop() override;

    void createConnection(int sockFd);
    void removeConnection(service::ServiceId connId);
    void acceptNewConnection();
    void removeStaleConnections();
    void handleFailedSockets(const utils::FdPoller::FileDescriptors& failedFds);
    void handleReadySockets(const utils::FdPoller::FileDescriptors& readyFds);
    void handleSocketMessage(int sockFd);
    void dispatchMessage(std::unique_ptr<message::Msg> msg, int sockFd);
    void sendServerStartedNotif();
    void sendConnectionStatusNotif(service::ServiceId connId, bool state);

    int _port;
    const int _maxConnections;
    log::Logger _log;
    IdToConnectionMap _connections;
    int _serverSockFd;
    service::ServiceId _connectionId;
    utils::FdPoller _fdPoller;
    FdToConnIdMap _fdToConnIdMap;
    ConnIdToFdMap _connIdToFdMap;
    ConnIdToAddress _connIdToAddress;
    message::MsgSender _msgSender;
};



}
