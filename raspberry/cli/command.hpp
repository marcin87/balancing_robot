#pragma once

#include "exception/robotException.hpp"
#include "message/msg.hpp"
#include <vector>
#include <string>

namespace cli {

class CommandException : public exception::RobotException
{
public:
    CommandException(const std::string& message) :
	RobotException("[Command] " + message)
    {
    
    }

};

class Command
{
public:
    using Args = std::vector<std::string>;
    Command(const std::string& cmdName, const Args& args) : 
	_name(cmdName), _args(args), _isDone(false) { }
    virtual void execute() = 0;
    virtual void handleMsg(message::Msg& msg) = 0;
    bool isDone() const { return _isDone; } 
    void setDone(bool isDone) { _isDone = isDone; }
protected:
    const std::string _name;
    Args _args;
    bool _isDone;
};

}
