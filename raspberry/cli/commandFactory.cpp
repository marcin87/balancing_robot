#include "commandFactory.hpp"
#include "commands/getFreeHeapCmd.hpp"
#include "commands/getLogsCmd.hpp"
#include "commands/setMctrlPeriodCmd.hpp"
#include "commands/setPidDirectionCmd.hpp"
#include "commands/setPidParamCmd.hpp"
#include "message/msgSender.hpp"


namespace cli {

std::unique_ptr<Command> CommandFactory::createCommand(const std::string& cmdName, const Command::Args& args, message::MsgSender& sender)
{
    if(cmdName == "getFreeHeap")
		return std::make_unique<GetFreeHeapCmd>(cmdName, args, sender);

	if(cmdName == "getLogs")
		return std::make_unique<GetLogsCmd>(cmdName, args, sender);

	if(cmdName == "setMctrlPeriod")
		return std::make_unique<SetMctrlPeriodCmd>(cmdName, args, sender);

	if(cmdName == "setPidParam")
		return std::make_unique<SetPidParamCmd>(cmdName, args, sender);

	if(cmdName == "setPidDirection")
		return std::make_unique<SetPidDirectionCmd>(cmdName, args, sender);

    throw CommandException("Unrecognized command: " + cmdName);
}

}
