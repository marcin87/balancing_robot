#include "cli.hpp"
#include "config/configAccess.hpp"
#include "exception/robotException.hpp"
#include "service/addressFinder.hpp"
#include "message/msg.hpp"
#include "messages.h"
#include "commandFactory.hpp"
#include <sstream>

namespace {

class CliException : public exception::RobotException
{
public:
    CliException(const std::string& message) :
	RobotException("[Cli] " + message)
    {
    
    }

};

} // anonymous namespace

namespace cli{


Cli::Cli(const std::string& name, service::ServiceId serviceId, std::chrono::milliseconds msgWaitTime) :
    service::Service(name, serviceId, msgWaitTime),
    _log(name),
    _msgSender(getId()),
    _command(nullptr)
{
}

Cli::~Cli() = default;


void Cli::doRun()
{
}

void Cli::doStart()
{
    _log.info("Starting cli service");

}

void Cli::doStop()
{
    _log.info("Stopping cli service");

}

void Cli::handleUnregisteredMsg(std::unique_ptr<message::Msg> msg)
{
    if(_command)
    {
	_command->handleMsg(*msg);
    }
    else
    {
	_log.error("Received msgId=%d. No active command to handle it", msg->getMsgId());
    }
}

}
