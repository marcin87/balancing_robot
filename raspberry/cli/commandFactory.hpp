#pragma once

#include "command.hpp"
#include <string>
#include <memory>

namespace message {
class MsgSender;
}

namespace cli {

class CommandFactory
{
public:
    std::unique_ptr<Command> createCommand(const std::string& cmdName, const Command::Args& args, message::MsgSender& sender);
};

}
