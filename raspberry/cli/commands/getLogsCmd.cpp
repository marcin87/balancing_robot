#include "getLogsCmd.hpp"
#include "messages.h"
#include "service/addressFinder.hpp"
#include "message/msgSender.hpp"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <iterator>

namespace {
std::string logLevelToStr(int logLevel)
{
    switch(logLevel)
    {
    case 0x01:
        return "Info";
    case 0x02:
        return "Warning";
    case 0x04:
        return "Error";
    case 0x08:
        return "Debug";
    default:
	break;
    }

    return std::string("Unknown flag( ") + std::to_string(static_cast<int>(logLevel)) + " )";
}

std::string componentToStr(int component)
{
    switch(component)
    {
    case 0:
        return "Log_Robot";
    case 1:
        return "Log_Motors";
    case 2:
        return "Log_Encoders";
    case 3:
        return "Log_TcpServer";
    case 4:
        return "Log_TcpServerHandler";
    case 5:
        return "Log_Wlan";
    case 6:
        return "Log_GpioExpander";
    case 7:
        return "Log_I2CManager";
    case 8:
        return "Log_I2CTask";
    case 9:
        return "Log_Wheels";
    case 10:
        return "Log_ServerSpiCom";
    case 11:
        return "Log_Leds";
    case 12:
        return "Log_Updater";
    case 13:
        return "Log_Mpu";
    case 14:
        return "Log_MotionCtrl";
    default:
	break;
    }

    return std::string("Unknown flag( ") + std::to_string(static_cast<int>(component)) + " )";
}

std::string millisToTimeString(unsigned milliseconds)
{
    unsigned fraction = milliseconds % 1000;
    unsigned seconds = (milliseconds / 1000) % 60 ;
    unsigned minutes = (milliseconds / (1000*60)) % 60;
    unsigned hours   = (milliseconds / (1000*60*60)) % 24;

    std::stringstream ss;
    ss << std::setfill('0') << std::setw(2) << hours << ':';
    ss << std::setfill('0') << std::setw(2) << minutes << ':';
    ss << std::setfill('0') << std::setw(2) << seconds << '.';
    ss << std::setfill('0') << std::setw(3) << fraction;

    return ss.str();
}

bool sortLine(const std::string& str1, const std::string& str2)
{
    unsigned lineNum1 = 0, lineNum2 = 0;
    std::stringstream ss;
    ss.str(str1);
    ss >> lineNum1;
    ss.clear();
    ss.str(str2);
    ss >> lineNum2;
    return lineNum1 < lineNum2;
}


}


namespace cli {

GetLogsCmd::GetLogsCmd(const std::string& cmdName, const Args& args, message::MsgSender& sender) :
    Command(cmdName, args),
    _sender(sender)
{
    if(!service::findServiceAddress("connection", _connectionAddress))
	throw CommandException("Could not find connection address");
}

void GetLogsCmd::execute()
{
    if(_args.empty())
    {
	std::cerr << "GetLogsCmd: master/slave parameter not specified" << std::endl;
	setDone(true);
        return;
    }

    std::string argument(_args.front());

    if(argument != "0" && argument != "1")
    {
	std::cerr << "GetLogsCmd: improper master/slave parameter" << std::endl;
	setDone(true);
        return;
    }

    sendMsgRequest();
}

void GetLogsCmd::sendMsgRequest()
{
    message::DataMsg<GetLogsMsgReq> msg(INIT_GET_LOGS_MSG_REQ);
    msg.getPayload().isMaster = 1;

    _sender.send(std::move(msg), _connectionAddress);
}

void GetLogsCmd::handleMsg(message::Msg& msg)
{
    if(msg.getMsgId() != GET_LOGS_MSG_RSP)
    {
	std::cerr << "Unrecognized msgId=" << msg.getMsgId() << std::endl;
	setDone(true);
	return;
    }
    
    auto& response = msg.getPayload<GetLogsMsgRsp>();

    std::string line;
    uint16_t totalLineNumber = response.totalLineNum;
    
    if(!response.status)
    {
	std::cerr << "Returned status NOK" << std::endl;
	setDone(true);
    }

    line += std::to_string(static_cast<int>(response.lineNum));
    line += millisToTimeString(response.timestamp) + " ";
    line += logLevelToStr(response.logLevel) + " ";
    line += componentToStr(response.component) + " ";

    std::string strTemp(reinterpret_cast<const char*>(response.strBuffer));
    const uint8_t* argsBuffer = reinterpret_cast<const uint8_t*>(&response.argsBuffer[0]);
    const size_t FORMAT_BUFF_SIZE = 200;
    char formatBuffer[FORMAT_BUFF_SIZE] = {0};
    size_t charsNum = 0;

    const uint8_t DOUBLE_ARG = 2;

    const uint8_t* argsBufferPtr = argsBuffer;

    size_t pos = strTemp.find('%');

    if(pos != std::string::npos)
    {
        line += strTemp.substr(0, pos);

        for(int i = 0; i != response.argsNum; ++i)
        {
            size_t tempPos = strTemp.find('%', pos + 1);

            if(tempPos != std::string::npos)
            {
                if(tempPos - pos)
                {
                    if(response.argTypes[i] == DOUBLE_ARG)
                    {
                        charsNum  = snprintf(formatBuffer,
                                        FORMAT_BUFF_SIZE,
                                        strTemp.substr(pos, tempPos - pos).c_str(),
                                        *((double*)argsBufferPtr)
                        );

                        argsBufferPtr += sizeof(double);
                    }
                    else
                    {
                        charsNum  = snprintf(formatBuffer,
                                        FORMAT_BUFF_SIZE,
                                        strTemp.substr(pos, tempPos - pos).c_str(),
                                        *((unsigned*)argsBufferPtr)
                        );

                        argsBufferPtr += sizeof(unsigned);
                    }

                    line += std::string(formatBuffer).substr(0, charsNum);
                }
                pos = tempPos;
            }
            else
            {
                if(response.argTypes[i] == DOUBLE_ARG)
                {
                    charsNum  = snprintf(formatBuffer,
                            FORMAT_BUFF_SIZE,
                            strTemp.substr(pos).c_str(),
                            *((double*)argsBufferPtr)
                    );

                    argsBufferPtr += sizeof(double);
                }
                else
                {
                    charsNum  = snprintf(formatBuffer,
                            FORMAT_BUFF_SIZE,
                            strTemp.substr(pos).c_str(),
                            *((unsigned*)argsBufferPtr)
                    );

                    argsBufferPtr += sizeof(unsigned);
                }

                line += std::string(formatBuffer).substr(0, charsNum);
                break;
            }

        }
    }
    else
    {
        line += strTemp;
    }


    _logLines.push_back(line);
    std::cout << line << std::endl;

    if(_logLines.size() == totalLineNumber)
    {
//	std::sort(_logLines.begin(), _logLines.end(), sortLine);
//	std::copy(_logLines.cbegin(), _logLines.cend(), std::ostream_iterator<std::string>(std::cout, "\n"));
        setDone(true);
    }
    else
    {
	sendMsgRequest();
    }
}

}
