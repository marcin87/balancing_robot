#include "setMctrlPeriodCmd.hpp"
#include "messages.h"
#include "service/addressFinder.hpp"
#include "message/msgSender.hpp"
#include <iostream>
#include <sstream>

namespace cli {

SetMctrlPeriodCmd::SetMctrlPeriodCmd(const std::string& cmdName, const Args& args, message::MsgSender& sender) :
    Command(cmdName, args),
    _sender(sender)
{
	if(!service::findServiceAddress("connection", _connectionAddress))
		throw CommandException("Could not find connection address");
}

void SetMctrlPeriodCmd::execute()
{
    if(_args.empty())
    {
		std::cout << "Usage: setMctrlPeriod <periodMs>" << std::endl;
		setDone(true);
        return;
    }

	std::stringstream ss;
    unsigned period;

    ss << _args.at(0);
    ss >> period;

    if(ss.fail())
    {
		std::cout << "Incorrect argument: \"" << _args.at(0) << "\"" << std::endl;
		setDone(true);
        return;
    }

	message::DataMsg<MctrlSetPeriodMsgReq> msg(INIT_MCTRL_SET_PERIOD_MSG_REQ);
	msg.getPayload().periodMs = static_cast<unsigned char>(period);

	_sender.send(std::move(msg), _connectionAddress);
}

void SetMctrlPeriodCmd::handleMsg(message::Msg& msg)
{
	if(msg.getMsgId() != MCTRL_SET_PERIOD_MSG_RSP)
	{
		std::cerr << "Unrecognized msgId=" << msg.getMsgId() << std::endl;
		setDone(true);
		return;
	}

	auto& payload = msg.getPayload<MctrlSetPeriodMsgRsp>();

    if(!payload.status)
    {
		std::cout << "SetMctrlPeriodCmd: couldn't set the motion control period" << std::endl;
    }
    else
    {
		std::cout << "Period set successfully" << std::endl;
    }

	setDone(true);
}

}
