#pragma once

#include "command.hpp"
#include "message/address.hpp"
#include <vector>
#include <string>

namespace message {
class MsgSender;
}

namespace cli {

class GetLogsCmd : public Command
{
public:  
    GetLogsCmd(const std::string& cmdName, const Args& args, message::MsgSender& sender);    
    virtual void execute() override;
    virtual void handleMsg(message::Msg& msg) override;

private:
    void sendMsgRequest();

    message::MsgSender& _sender;
    message::Address _connectionAddress;
    std::vector<std::string> _logLines;
};

}
