#include "getFreeHeapCmd.hpp"
#include "messages.h"
#include "service/addressFinder.hpp"
#include "message/msgSender.hpp"
#include <iostream>

namespace cli {

GetFreeHeapCmd::GetFreeHeapCmd(const std::string& cmdName, const Args& args, message::MsgSender& sender) :
    Command(cmdName, args),
    _sender(sender)
{
    if(!service::findServiceAddress("connection", _connectionAddress))
	throw CommandException("Could not find connection address");
}

void GetFreeHeapCmd::execute()
{
    if(_args.empty())
    {
	std::cerr << "GetFreeHeapSizeCommand: master/slave parameter not specified" << std::endl;
	setDone(true);
        return;
    }

    std::string argument(_args.front());

    if(argument != "0" && argument != "1")
    {
	std::cerr << "GetFreeHeapSizeCommand: improper master/slave parameter" << std::endl;
	setDone(true);
        return;
    }

    bool isMaster = (argument == "0") ? false : true;

    message::DataMsg<GetFreeHeapSizeReq> msg(INIT_GET_FREE_HEAP_SIZE_MSG_REQ);
    msg.getPayload().isMaster = 1;

    _sender.send(std::move(msg), _connectionAddress);
}

void GetFreeHeapCmd::handleMsg(message::Msg& msg)
{
    if(msg.getMsgId() != GET_FREE_HEAP_SIZE_MSG_RSP)
    {
	std::cerr << "Unrecognized msgId=" << msg.getMsgId() << std::endl;
	setDone(true);
	return;
    }
    
    auto& payload = msg.getPayload<GetFreeHeapSizeRsp>();

    std::cout << "Free heap size: " << payload.heapSize << " words." << std::endl;
    setDone(true);
}

}
