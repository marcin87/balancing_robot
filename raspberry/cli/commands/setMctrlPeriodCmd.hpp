#pragma once

#include "command.hpp"
#include "message/address.hpp"

namespace message {
class MsgSender;
}

namespace cli {

class SetMctrlPeriodCmd : public Command
{
public:  
    SetMctrlPeriodCmd(const std::string& cmdName, const Args& args, message::MsgSender& sender);    
    virtual void execute() override;
    virtual void handleMsg(message::Msg& msg) override;

private:
    message::MsgSender& _sender;
    message::Address _connectionAddress;
};

}
