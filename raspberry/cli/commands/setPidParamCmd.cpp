#include "setPidParamCmd.hpp"
#include "messages.h"
#include "service/addressFinder.hpp"
#include "message/msgSender.hpp"
#include <iostream>
#include <sstream>

namespace cli {

SetPidParamCmd::SetPidParamCmd(const std::string& cmdName, const Args& args, message::MsgSender& sender) :
    Command(cmdName, args),
    _sender(sender)
{
	if(!service::findServiceAddress("connection", _connectionAddress))
		throw CommandException("Could not find connection address");
}

void SetPidParamCmd::execute()
{
    if(_args.empty())
    {
		std::cout << "Usage: setPidParam <param> <value>" << std::endl;
		std::cout << "\tparam: {0 - Proportional, 1 - Integral, 2 - Derivative, 3 - SetPoint}" << std::endl;
		setDone(true);
        return;
    }

	std::stringstream ss;
    unsigned param;
    float value;

    ss << _args.at(0);
    ss >> param;

    if(ss.fail())
    {
		std::cout << "Incorrect argument: \"" << _args.at(0) << "\"" << std::endl;
		setDone(true);
        return;
    }

    ss.str("");
    ss.clear();

    ss << _args.at(1);
    ss >> value;

    if(ss.fail())
    {
		std::cout << "Incorrect argument: \"" << _args.at(1) << "\"" << std::endl;
		setDone(true);
        return;
    }

	message::DataMsg<MctrlSetPidParamMsgReq> msg(INIT_MCTRL_SET_PID_PARAM_MSG_REQ);
	msg.getPayload().param = static_cast<unsigned char>(param);
	msg.getPayload().value = value;

	_sender.send(std::move(msg), _connectionAddress);
}

void SetPidParamCmd::handleMsg(message::Msg& msg)
{
	if(msg.getMsgId() != MCTRL_SET_PID_PARAM_MSG_RSP)
	{
		std::cerr << "Unrecognized msgId=" << msg.getMsgId() << std::endl;
		setDone(true);
		return;
	}

	auto& payload = msg.getPayload<MctrlSetPidParamMsgRsp>();

    if(!payload.status)
    {
		std::cout << "SetPidParamCmd: couldn't set the parameter" << std::endl;
    }
    else
    {
		std::cout << "Parameter set successfully" << std::endl;
    }

	setDone(true);
}

}
