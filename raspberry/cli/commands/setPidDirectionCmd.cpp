#include "setPidDirectionCmd.hpp"
#include "messages.h"
#include "service/addressFinder.hpp"
#include "message/msgSender.hpp"
#include <iostream>
#include <sstream>

namespace cli {

SetPidDirectionCmd::SetPidDirectionCmd(const std::string& cmdName, const Args& args, message::MsgSender& sender) :
    Command(cmdName, args),
    _sender(sender)
{
	if(!service::findServiceAddress("connection", _connectionAddress))
		throw CommandException("Could not find connection address");
}

void SetPidDirectionCmd::execute()
{
    if(_args.empty())
    {
		std::cout << "Usage: setPidDirection <direction>" << std::endl;
		std::cout << "\tdirection: {0 - Direct, 1 - Reverse}" << std::endl;
		setDone(true);
        return;
    }

	std::stringstream ss;
    unsigned direction;

    ss << _args.at(0);
    ss >> direction;

    if(ss.fail())
    {
		std::cout << "Incorrect argument: \"" << _args.at(0) << "\"" << std::endl;
		setDone(true);
        return;
    }

	message::DataMsg<MctrlSetPidDirMsgReq> msg(INIT_MCTRL_SET_PID_DIR_MSG_REQ);
	msg.getPayload().direction = static_cast<unsigned char>(direction);

	_sender.send(std::move(msg), _connectionAddress);
}

void SetPidDirectionCmd::handleMsg(message::Msg& msg)
{
	if(msg.getMsgId() != MCTRL_SET_PID_DIR_MSG_RSP)
	{
		std::cerr << "Unrecognized msgId=" << msg.getMsgId() << std::endl;
		setDone(true);
		return;
	}

	auto& payload = msg.getPayload<MctrlSetPidDirMsgRsp>();

    if(!payload.status)
    {
		std::cout << "SetPidDirectionCmd: couldn't set the PID direction" << std::endl;
    }
    else
    {
		std::cout << "Direction set successfully" << std::endl;
    }

	setDone(true);
}

}
