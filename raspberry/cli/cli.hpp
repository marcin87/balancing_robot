#pragma once

#include "service/service.hpp"
#include "logger/logger.hpp"
#include "utils/fdPoller.hpp"
#include "message/msgSender.hpp"
#include "command.hpp"
#include <string>
#include <memory>

namespace message {
class Msg;
}

namespace cli {


class Cli : public service::Service
{
public:
    Cli(const std::string& name,
	   service::ServiceId serviceId,
	   std::chrono::milliseconds msgWaitTime = std::chrono::milliseconds(0));
    ~Cli();
protected:

    virtual void doRun() override;
    virtual void doStart() override;
    virtual void doStop() override;
    virtual void handleUnregisteredMsg(std::unique_ptr<message::Msg> msg) override;
    
    message::Address _connectionAddres;
    log::Logger _log;
    message::MsgSender _msgSender; 
    std::unique_ptr<Command> _command;
};


}
