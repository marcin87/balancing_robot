#include "cli.hpp"
#include "config/configAccess.hpp"
#include "exception/robotException.hpp"
#include "service/addressFinder.hpp"
#include "message/msg.hpp"
#include "messages.h"
#include "commandFactory.hpp"
#include <readline/readline.h>
#include <readline/history.h>
#include <sstream>

namespace {

constexpr const char* historyFileName = "cmd_history";

class CliException : public exception::RobotException
{
public:
    CliException(const std::string& message) :
	RobotException("[Cli] " + message)
    {
    
    }

};

} // anonymous namespace

namespace cli{


Cli::Cli(const std::string& name, service::ServiceId serviceId, std::chrono::milliseconds msgWaitTime) :
    service::Service(name, serviceId, msgWaitTime),
    _log(name),
    _msgSender(getId()),
    _command(nullptr)
{
}

Cli::~Cli() = default;


void Cli::doRun()
{
    if(_command && !_command->isDone())
	return;

    auto input = ::readline("command: ");

    if (!input)
	return;

    std::string line(input);
    std::string cmdName, argument;
    Command::Args args;

    std::stringstream ss(line);
    ss >> cmdName;

    while(ss >> argument)
    {
	args.push_back(argument);
    }

    if(line == "q" || line == "quit")
    {
	::free(input);
	stop();
	return;
    }
    
    try 
    {
	_command = CommandFactory().createCommand(cmdName, args, _msgSender);
    }
    catch(const CliException& exception)
    {
	::free(input);
	std::cerr << "Exception: "  << exception.what() << std::endl;
	return;
    }

    ::add_history(input);
    _command->execute();

    ::free(input);
    std::cout << std::endl;
}

void Cli::doStart()
{
    _log.info("Starting cli service");

    ::using_history();
    ::rl_bind_key('\t', rl_complete);
    ::read_history(historyFileName);
}

void Cli::doStop()
{
    _log.info("Stopping cli service");

    ::write_history(historyFileName);
}

void Cli::handleUnregisteredMsg(std::unique_ptr<message::Msg> msg)
{
    if(_command)
    {
	_command->handleMsg(*msg);
    }
    else
    {
	_log.error("Received msgId=%d. No active command to handle it", msg->getMsgId());
    }
}

}
