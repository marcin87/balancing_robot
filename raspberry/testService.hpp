#pragma once
#include <iostream>
#include <fstream>
#include "service/service.hpp"
#include "messages.h"
#include "message/msg.hpp"
#include "service/addressFinder.hpp"
#include "message/address.hpp"
#include "logger/logger.hpp"
#include "message/msgSender.hpp"

namespace test {

class TestClient : public service::Service
{
public:
    TestClient(const std::string& name,
	      service::ServiceId serviceId,
	      std::chrono::milliseconds msgWaitTime = std::chrono::milliseconds(0));
protected:
    virtual void doRun() override;
    void sendTestMsg();

    log::Logger _log;
    message::Address _clientServAddr;
    message::MsgSender _sender;
};

class TestServer : public service::Service
{
public:
    TestServer(const std::string& name,
	      service::ServiceId serviceId,
	      std::chrono::milliseconds msgWaitTime = std::chrono::milliseconds(0));
protected:
    virtual void doRun() override;

    log::Logger _log;
    message::MsgSender _sender;
};



}

