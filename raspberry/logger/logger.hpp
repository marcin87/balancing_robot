#pragma once

#include <iostream>
#include <string>
#include <memory>

namespace log {

enum class Severity {info, debug, warning, error};

class LogSettings
{
public:
    static void setSeverity(const Severity& severity);
    static void unsetSeverity(const Severity& severity);
    static bool isSeveritySet(const Severity& severity);
};

class Logger
{
public: 
    Logger(const std::string& componentName);

    template<typename ... Args>
    void info(const std::string& format, Args ... args)
    {
	log(Severity::info, format, args ...);
    }

    template<typename ... Args>
    void debug(const std::string& format, Args ... args)
    {
	log(Severity::debug, format, args ...);
    }

    template<typename ... Args>
    void warning(const std::string& format, Args ... args)
    {
	log(Severity::warning, format, args ...);
    }

    template<typename ... Args>
    void error(const std::string& format, Args ... args)
    {
	log(Severity::error, format, args ...);
    }


protected:
    template<typename ... Args>
    void log(const Severity& severity, const std::string& format, Args ... args)
    {
	if(!LogSettings::isSeveritySet(severity))
	    return;

	size_t size = snprintf(nullptr, 0, format.c_str(), args ...) + 1; // Extra space for '\0'
	std::unique_ptr<char[]> buf(std::make_unique<char[]>(size)); 
	snprintf(buf.get(), size, format.c_str(), args ...);
	std::string strLog(buf.get(), buf.get() + size - 1);
	doLog(severity, strLog); // We don't want the '\0' inside
    
    }

    void doLog(const Severity& severity, const std::string& logStr);


    const std::string _componentName;
};

}
