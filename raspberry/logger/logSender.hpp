#pragma once

#include <string>
#include <chrono>

namespace log {

class LogEntry
{
public:
    LogEntry() = default;
    LogEntry(const std::string& logStr);
    LogEntry(const LogEntry&) = default;
    LogEntry& operator=(const LogEntry&);
    LogEntry(LogEntry&&) = default;
    LogEntry& operator=(LogEntry&&);

    auto getTimestamp() const { return _timeStamp; }
    auto getLogStr() const {return _logStr; }
private:
    std::chrono::system_clock::time_point _timeStamp;
    std::string _logStr;
};


class LogSender
{
public:
    LogSender() = default;
    LogSender(const LogSender&) = delete;
    LogSender& operator=(const LogSender&) = delete;

    void send(const LogEntry& logEntry);
    bool receive(LogEntry& entry);

};

}
