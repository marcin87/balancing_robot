#include "logSender.hpp"
#include <mutex>
#include <queue>
#include <condition_variable>

namespace log {
namespace {
using LineQueue = std::queue<LogEntry>;
   
static LineQueue queue;
static std::mutex mutex;
static std::condition_variable cv;
}


LogEntry::LogEntry(const std::string& logStr) :
    _timeStamp(std::chrono::system_clock::now()),
    _logStr(logStr)
{

}

LogEntry& LogEntry::operator=(const LogEntry& rhs)
{
    _timeStamp = rhs._timeStamp;
    _logStr = rhs._logStr;

    return *this;
}

LogEntry& LogEntry::operator=(LogEntry&& rhs)
{
    _timeStamp = std::move(rhs._timeStamp);
    _logStr = std::move(rhs._logStr);

    return *this;
}


void LogSender::send(const LogEntry& logEntry)
{
    std::unique_lock<std::mutex> lck(mutex);
    queue.emplace(logEntry);
    cv.notify_one();
}

bool LogSender::receive(LogEntry& entry)
{
    std::unique_lock<std::mutex> lck(mutex);

    if(queue.empty() && cv.wait_for(lck, std::chrono::seconds(1)) == std::cv_status::timeout)
	return false;

    entry = std::move(queue.front());
    queue.pop();

    return true;
}

}
