#include "logger.hpp"
#include "logSender.hpp"
#include <mutex>
#include <unordered_map>
#include <fstream>
#include <algorithm>
#include <chrono>

namespace log {

namespace {

  
static std::mutex mutex;
static std::unordered_map<Severity, bool> severities{
	{Severity::info, true},
	{Severity::debug, false},
	{Severity::warning, true},
	{Severity::error, true}
    };

std::string severityToSTr(const Severity& severity)
{
    std::string strSeverity;

    switch(severity)
    {
    case Severity::info:
	strSeverity = "INF";
	break;
    case Severity::debug:
	strSeverity = "DBG";
	break;
    case Severity::warning:
	strSeverity = "WRN";
	break;
    case Severity::error:
	strSeverity = "ERR";
	break;
    default:
	break;
    }
    
    return strSeverity;
}

} // anonymous namespace

void LogSettings::setSeverity(const Severity& severity)
{
    severities.at(severity) = true;
}

void LogSettings::unsetSeverity(const Severity& severity)
{
    severities.at(severity) = false;
}

bool LogSettings::isSeveritySet(const Severity& severity)
{
    return severities.at(severity);
}

Logger::Logger(const std::string& componentName) : 
    _componentName(componentName)
{
}

void Logger::doLog(const Severity& severity, const std::string& logStr)
{
    
    LogEntry entry(std::string("[") + severityToSTr(severity) + "] [" + _componentName + "] " + logStr);
    LogSender().send(entry);
}



}
