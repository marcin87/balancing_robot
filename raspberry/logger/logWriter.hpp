#pragma once
#include <iostream>
#include <fstream>
#include "service/service.hpp"

namespace log {

class LogWriter : public service::Service
{
public:
    LogWriter(const std::string& name,
	      service::ServiceId serviceId,
	      std::chrono::milliseconds msgWaitTime = std::chrono::milliseconds(0));
protected:
    virtual void doRun() override;

    std::ofstream _outStream;
};



}

