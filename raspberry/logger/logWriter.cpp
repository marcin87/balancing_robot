#include "logWriter.hpp"
#include "config/configAccess.hpp"
#include "logSender.hpp"
#include <mutex>
#include <unordered_map>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <chrono>
#include <ctime>

namespace log {

namespace {

std::string getLogFileName()
{
    auto conf = config::ConfigAccess().get();
    auto fileName =  conf->logger()->file()->str();
    return std::move(fileName);
}

}


LogWriter::LogWriter(const std::string& name,
		     service::ServiceId serviceId,
		     std::chrono::milliseconds msgWaitTime) :
    service::Service(name, serviceId, msgWaitTime),
    _outStream(getLogFileName())
{
}

void LogWriter::doRun()
{
    static unsigned lineCount = 1;
    LogEntry entry;

    while(LogSender().receive(entry))
    {

	auto timestamp = entry.getTimestamp();
	auto timestamp_c = std::chrono::system_clock::to_time_t(timestamp);
	auto ms = std::chrono::duration_cast<std::chrono::microseconds>(timestamp.time_since_epoch()) % 1000000;

	_outStream << lineCount++ << " | " << std::put_time(std::localtime(&timestamp_c), "%F %T") 
	    << "." << std::setw(6) << ms.count() << " | " << entry.getLogStr() << std::endl;

    }

}


}
