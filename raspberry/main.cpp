#include <iostream>
#include "logger/logWriter.hpp"
#include "logger/logger.hpp"
#include "server/server.hpp"
#include "serdes/serializer.hpp"
#include "serdes/deserializer.hpp"
#include "messages.h"
#include "message/msgQueue.hpp"
#include "service/addressFinder.hpp"
#include "msgSender.hpp"
#include "service/serviceManager.hpp"
#include <thread>
#include <chrono>

using namespace std::literals::chrono_literals;

int main()
{
    using log::Logger;
    using log::LogWriter;
    using server::Server;
    using log::LogSettings;
    using log::Severity;
    
    LogSettings::setSeverity(Severity::debug);
    LogSettings::setSeverity(Severity::warning);

    Logger logger("MAIN");
    service::ServiceManager serviceManager;
    serviceManager.start();
    auto initialServiceNum = serviceManager.serviceCount();

    while(initialServiceNum == serviceManager.serviceCount())
    {
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	serviceManager.cleanup();
    }


    logger.info("initialServiceNum=%u, finalServiceCount=%u", initialServiceNum, serviceManager.serviceCount());
    
    return 0;
}
