#include "connection.hpp"
#include "config/configAccess.hpp"

namespace {

auto getLocalMappings()
{
    auto conf = config::ConfigAccess().get();
    auto mappingVec =  conf->connection()->msgRouter()->localReqMappings();
    connection::MsgRouter::MsgToRecipientMappings result;

    for(const auto& mapping : *mappingVec)
	result.emplace_back(mapping->msgId(), mapping->serviceId());

    return std::move(result);
}

auto getRemoteMsgIds()
{
    auto conf = config::ConfigAccess().get();
    auto msgIdVec =  conf->connection()->msgRouter()->remoteReqIds();
    connection::MsgRouter::MsgIds result(msgIdVec->begin(), msgIdVec->end());

    return std::move(result);
}

}

namespace connection {

Connection::Connection(const std::string& name,
	    service::ServiceId serviceId,
	    std::chrono::milliseconds msgWaitTime,
	    int sockFd) :
    service::Service(name, serviceId, msgWaitTime),
    _msgSender(serviceId),
    _sockFd(sockFd),
    _socketWriter(sockFd),
    _log(name),
    _router(getLocalMappings(), getRemoteMsgIds(), _msgSender, _socketWriter, _log)
{
    _log.info("Connection \"%s\" created, fd: %d", name.c_str(), sockFd);
}

void Connection::doRun()
{


}

void Connection::handleUnregisteredMsg(std::unique_ptr<message::Msg> msg)
{
    _router.routeMsg(std::move(msg));
}

}
