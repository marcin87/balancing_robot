#include "connection/msgRouter.hpp"
#include "serdes/serializer.hpp"
#include "exception/robotException.hpp"
#include "utils/fdWriter.hpp"
#include "messages.h"

namespace {

class MsgRouterException : public exception::RobotException
{
public:
    MsgRouterException(const std::string& message) :
	RobotException("[MsgRouter] " + message)
    {
    
    }

};


}


namespace connection {

MsgRouter::MsgRouter(const MsgToRecipientMappings& reqLocalMapping,
		     const MsgIds& reqRemoteMsgs,
		     message::MsgSender& msgSender,
		     utils::FdWriter& socketWriter,
		     log::Logger& logger) :
    _reqLocalRt(reqLocalMapping.cbegin(), reqLocalMapping.cend()),
    _reqRemoteRt(reqRemoteMsgs.cbegin(), reqRemoteMsgs.cend()),
    _msgSender(msgSender),
    _socketWriter(socketWriter),
    _log(logger)
{
    _log.debug("Read message mappings:");

    for(const auto& localMapping : _reqLocalRt)
    {
	if(_reqRemoteRt.find(localMapping.first) != _reqRemoteRt.end())
	    throw MsgRouterException("Request MsgId=" + std::to_string(localMapping.first) + 
		    " exists in both remote and local routing tables");

	_log.debug("Local mapping: {msgId=%d, serviceId=%d}", localMapping.first, localMapping.second);
    }

    for(const auto& remoteRequest : _reqRemoteRt)
    {
	_log.debug("Remote requests: {msgId=%d}", remoteRequest); 
    }

    _log.debug("MsgRouter created");
}

void MsgRouter::routeMsg(std::unique_ptr<message::Msg> msg)
{
    auto msgId = msg->getMsgId();
    auto sender = msg->getSender();
    
    _log.debug("Routing message id=%d from sender %u", msgId, sender);

    auto reqRemote = _reqRemoteRt.find(msgId);
    
    if(reqRemote != _reqRemoteRt.end())
    {
	updateRespRt(_respRemoteRt, msgId, sender);
	sendRemoteMsg(std::move(msg));
	_log.debug("Message request routed to remote host");
	return;
    }

    auto reqLocalMapping = _reqLocalRt.find(msgId);

    if(reqLocalMapping != _reqLocalRt.end())
    {
	updateRespRt(_respLocalRt, msgId, sender);
	message::Address address {reqLocalMapping->second};
	_msgSender.send(std::move(msg), address);
	_log.debug("Message request routed to local recepient id=%u", address.queueId);
	return;
    }

    auto respMapping = _respRemoteRt.find(msgId);

    if(respMapping != _respRemoteRt.end())
    {
	if(respMapping->second.empty())
	    throw MsgRouterException("No local recepients awaiting for "
		    "response msgId=" + std::to_string(msgId) +
		    ", sent from serviceId=" + std::to_string(sender));

	auto recepient = respMapping->second.back();
	respMapping->second.pop();
	message::Address address {recepient};
	_msgSender.send(std::move(msg), address);
	_log.debug("Message response routed to local recepient id=%u", address.queueId);
	return;
    }

    respMapping = _respLocalRt.find(msgId);

    if(respMapping != _respLocalRt.end())
    {
	if(respMapping->second.empty())
	    throw MsgRouterException("No remote recepients awaiting for "
		    "response msgId=" + std::to_string(msgId) + ", sent from serviceId=" +
		    std::to_string(sender));

	respMapping->second.pop();
	sendRemoteMsg(std::move(msg));
	_log.debug("Message response routed to remote host");
	return;

    }

    throw MsgRouterException("Could not route message id=" + std::to_string(msgId) +
	    ". Given id not present in any of the routing tables");
}

void MsgRouter::updateRespRt(RespRoutingTable& routingTable, MsgId msgId, RecepientId recepient)
{
    auto respId = getResponseId(msgId);
    
    if(respId < 0)
	throw MsgRouterException("Could not update response routing table."
		" There is no response id for request id=" + std::to_string(msgId));

    auto respMapping = routingTable.find(respId);

    if(respMapping == routingTable.end())
    {
	RecepientIds recepients;
	recepients.push(recepient);
	routingTable.emplace(respId, std::move(recepients));
    }
    else
    {
	respMapping->second.push(recepient);	
    }
}

void MsgRouter::sendRemoteMsg(std::unique_ptr<message::Msg> msg)
{
    serialization::Serializer serializer;
    serializer.serialize(msg->getPayloadRawBuffer(), msg->getMsgId());
    _socketWriter.write(reinterpret_cast<char*>(serializer.getBuffer()), serializer.getBufferSize());
}


}
