#pragma once

#include "service/service.hpp"
#include "message/msg.hpp"
#include "message/msgSender.hpp"
#include "logger/logger.hpp"
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <queue>

namespace utils {
    class FdWriter;
}


namespace connection {

class MsgRouter
{
public:
    using MsgId = int;
    using RecepientId = service::ServiceId;
    using MsgToRecipientMappings = std::vector<std::pair<MsgId, RecepientId>>;
    using MsgIds = std::vector<MsgId>;

    MsgRouter(const MsgToRecipientMappings& reqLocalMapping,
	      const MsgIds& reqRemoteMsgs,
	      message::MsgSender& msgSender,
	      utils::FdWriter& socketWriter,
	      log::Logger& logger);

    void routeMsg(std::unique_ptr<message::Msg> msg);

protected:
    using RecepientIds = std::queue<RecepientId>; 
    using ReqRoutingTable = std::unordered_map<MsgId, RecepientId>; 
    using RespRoutingTable = std::unordered_map<MsgId, RecepientIds>; 
    using MsgIdSet = std::unordered_set<MsgId>;

    void updateRespRt(RespRoutingTable& routingTable, MsgId msgId, RecepientId recepient);
    void sendRemoteMsg(std::unique_ptr<message::Msg> msg);

    ReqRoutingTable _reqLocalRt;
    MsgIdSet _reqRemoteRt;
    RespRoutingTable _respLocalRt;
    RespRoutingTable _respRemoteRt;
    message::MsgSender& _msgSender;
    utils::FdWriter& _socketWriter;
    log::Logger _log;
};

}
