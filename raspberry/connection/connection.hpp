# pragma once
#include "msgRouter.hpp"
#include "logger/logger.hpp"
#include "service/service.hpp"
#include "message/msgSender.hpp"
#include "utils/fdWriter.hpp"
#include <unordered_map>
#include <queue>
#include <memory>

namespace connection {

class  Connection : public service::Service
{
public:
    Connection(const std::string& name,
	    service::ServiceId serviceId,
	    std::chrono::milliseconds msgWaitTime,
	    int sockFd);   
protected:
    virtual void doRun() override;
    virtual void handleUnregisteredMsg(std::unique_ptr<message::Msg> msg) override;

    message::MsgSender _msgSender;
    int _sockFd;
    utils::FdWriter _socketWriter;
    log::Logger _log;
    MsgRouter _router;
};


}
