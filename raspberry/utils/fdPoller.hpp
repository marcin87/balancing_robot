#pragma once

#include <vector>
#include <chrono>
#include <string>
#include <poll.h>

namespace utils {

class FdPoller
{
public:
    enum class PollType {
	READ,
	WRITE
    };

    enum class FdFault {
	EXCEPTION,
	ERROR,
	HUNGUP,
	INVALID_REQUEST
    };

    using FileDescriptors = std::vector<int>;
    using FdFaults = std::vector<FdFault>;

    FdPoller(PollType pollType,
	     std::chrono::milliseconds timeout,
	     const FileDescriptors& fds = FileDescriptors());

    void addFd(int fileDescriptor);
    void removeFd(int fileDescriptor);
    bool poll();
    FileDescriptors getReadyFds() const;
    FileDescriptors getFailedFds() const;
    FdFaults getFdFaults(int fileDescriptor) const;
private:
    PollType _pollType;
    std::chrono::milliseconds _timeout;
    std::vector<pollfd> _pollFds;
};

class ReadFdPoller : public FdPoller
{
public:
    ReadFdPoller(std::chrono::milliseconds timeout,
		 const FileDescriptors& fds = FileDescriptors())
	: FdPoller(PollType::READ, timeout, fds) { }

};

class WriteFdPoller : public FdPoller
{
public:
    WriteFdPoller(std::chrono::milliseconds timeout,
		 const FileDescriptors& fds = FileDescriptors())
	: FdPoller(PollType::WRITE, timeout, fds) { }

};

std::string fdFaultToStr(FdPoller::FdFault fault);

}
