#include "fdWriter.hpp"
#include "unistd.h"
#include "robotException.hpp"
#include <string.h>

namespace {

class FdWriterException : public exception::RobotException
{
public:
    FdWriterException(const std::string& message) :
	RobotException("[FdWriter] " + message)
    {
    
    }

};

}

namespace utils {

FdWriter::FdWriter(int fd) :
    _fd(fd)
{
    
}

void FdWriter::write(const char* data, unsigned size) const
{
    int leftToWrite = size;

    while(leftToWrite > 0)
    {
	auto result = ::write(_fd, data, leftToWrite);

	if(result < 0)
	    throw FdWriterException(strerror(errno));

	if(result == 0)
	{
	    throw FdWriterException("Could not write anymore. Bytes written: " + 
		    std::to_string(size - leftToWrite) + ", left: " + std::to_string(leftToWrite));
	    break;
	}

	leftToWrite -= result;
	data += result;
    }

}

}
