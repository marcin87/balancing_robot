#pragma once

#include <vector>
#include <chrono>

namespace utils {

class FdWriter
{
public:
    FdWriter(int fd);
    void write(const char* data, unsigned size) const;
private:
    int _fd;
};

}
