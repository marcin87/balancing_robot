#pragma once

#include <vector>
#include <chrono>

namespace utils {

class FdReader
{
public:
    FdReader(int fd);
    std::vector<char> read(unsigned size) const;
private:
    int _fd;
};

}
