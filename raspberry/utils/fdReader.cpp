#include "fdReader.hpp"
#include "unistd.h"
#include "robotException.hpp"
#include <string.h>

namespace {

class FdReaderException : public exception::RobotException
{
public:
    FdReaderException(const std::string& message) :
	RobotException("[FdReader] " + message)
    {
    
    }

};

}

namespace utils {

FdReader::FdReader(int fd) :
    _fd(fd)
{
    
}

std::vector<char> FdReader::read(unsigned size) const
{
    std::vector<char> buffer(size, 0);
   
    int leftToRead = size;
    while(leftToRead > 0)
    {
	auto result = ::read(_fd, buffer.data(), leftToRead);

	if(result < 0)
	    throw FdReaderException(strerror(errno));

	if(result == 0)
	{
	    buffer.resize(size - leftToRead);
	    break;
	}

	leftToRead -= result;
    }

    return buffer;
}

}
