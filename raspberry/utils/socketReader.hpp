#pragma once

#include <vector>

namespace utils {

std::vector<char> readSocketMsgBuffer(int sockFd);

}
