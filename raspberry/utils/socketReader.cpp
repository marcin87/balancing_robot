#include "socketReader.hpp"
#include "robotException.hpp"
#include "fdReader.hpp"

namespace {

class SocketReaderException : public exception::RobotException
{
public:
    SocketReaderException(const std::string& message) :
	RobotException("[SocketReader] " + message)
    {
    
    }

};


}


namespace utils {


std::vector<char> readSocketMsgBuffer(int sockFd)
{
    const unsigned PREFIX_SIZE = 4u;
    utils::FdReader reader(sockFd);
    auto buffer(std::move(reader.read(PREFIX_SIZE)));
    
    if(buffer.empty())
	return buffer;

    if(buffer.size() != PREFIX_SIZE)
	throw SocketReaderException("Could not read message buffer prefix. Size read: " 
		+ std::to_string(buffer.size()));

    auto bufferSize = *reinterpret_cast<unsigned*>(buffer.data());
    auto tempBuffer = reader.read(bufferSize);

    if(tempBuffer.size() != bufferSize)
	throw SocketReaderException("Could not read entire message buffer");

    buffer.insert(buffer.end(), tempBuffer.begin(), tempBuffer.end());
    return buffer;
}



}
