#include "fdPoller.hpp"
#include "robotException.hpp"
#include <string.h>
#include <algorithm>

namespace {

class FdPollerException : public exception::RobotException
{
public:
    FdPollerException(const std::string& message) :
	RobotException("[FdPoller] " + message)
    {
    
    }

};

void raiseResultException()
{
    throw FdPollerException(strerror(errno));
}

bool isEventFaulty(short event)
{
    switch(event)
    {
    case POLLPRI:
    case POLLERR:
    case POLLHUP:
    case POLLNVAL:
	return true;
    default:
	break;
    }

    return false;
}

}

namespace utils {

namespace {

inline short pollTypeToEvent(FdPoller::PollType pollType)
{
    return pollType == FdPoller::PollType::READ ? POLLIN : POLLOUT;
}

inline FdPoller::FdFaults getFaults(int revent)
{
    FdPoller::FdFaults faults;

    if(revent & POLLPRI)
	faults.push_back(FdPoller::FdFault::EXCEPTION);

    if(revent & POLLERR)
	faults.push_back(FdPoller::FdFault::ERROR);

    if(revent & POLLHUP)
	faults.push_back(FdPoller::FdFault::HUNGUP);

    if(revent & POLLNVAL)
	faults.push_back(FdPoller::FdFault::INVALID_REQUEST);
}

}

std::string fdFaultToStr(FdPoller::FdFault fault)
{
    switch(fault)
    {
    case FdPoller::FdFault::EXCEPTION:
	return "FdFault::EXCEPTION";
    case FdPoller::FdFault::ERROR:
	return "FdFault::ERROR";
    case FdPoller::FdFault::HUNGUP:
	return "FdFault::HUNGUP";
    case FdPoller::FdFault::INVALID_REQUEST:
	return "FdFault::INVALID_REQUEST";
    default:
	break;
    }
    
    return "UNKNOWN FAULT";
}

FdPoller::FdPoller(PollType pollType,
	           std::chrono::milliseconds timeout,
	           const FileDescriptors& fds) :
    _pollType(pollType),
    _timeout(timeout)
{
    for(const auto& fd : fds)
	addFd(fd);
}

void FdPoller::addFd(int fileDescriptor)
{
    auto events = pollTypeToEvent(_pollType);
    pollfd pfd{fileDescriptor, events, 0};
    _pollFds.push_back(pfd);
}

void FdPoller::removeFd(int fileDescriptor)
{
    _pollFds.erase(std::remove_if(_pollFds.begin(), _pollFds.end(),
		   [&](const auto& item) { return item.fd == fileDescriptor; } ),
		   _pollFds.end());
}

bool FdPoller::poll()
{
    auto result = ::poll(_pollFds.data(), _pollFds.size(), _timeout.count());

    if(result == 0)
	return false;

    if(result < 0)
	raiseResultException();
    
    return true; 
}

FdPoller::FileDescriptors FdPoller::getReadyFds() const
{
    FileDescriptors outFds;
    std::for_each(_pollFds.cbegin(), _pollFds.cend(),
	    [&](const auto& item) { 
		if(item.revents & pollTypeToEvent(_pollType)) outFds.push_back(item.fd); 
	    }
	    );

    return outFds;
}

FdPoller::FileDescriptors FdPoller::getFailedFds() const
{
    FileDescriptors outFds;
    std::for_each(_pollFds.cbegin(), _pollFds.cend(),
	    [&](const auto& item) { 
		if(isEventFaulty(item.revents)) outFds.push_back(item.fd);
	    }
	    );

    return outFds;
}

FdPoller::FdFaults FdPoller::getFdFaults(int fileDescriptor) const
{
    auto itemIter = std::find_if(_pollFds.cbegin(), _pollFds.cend(),
	    [&](const auto& item){ return item.fd == fileDescriptor; });

    return getFaults(itemIter->revents);
}

}
