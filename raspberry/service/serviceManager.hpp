#pragma once

#include <list>
#include <memory>
#include "logger/logger.hpp"

namespace service {

class Service;

class ServiceManager
{
public:
    ServiceManager();
    ~ServiceManager();

    void start();    
    void stop();
    void cleanup();
    unsigned serviceCount() const;
private:
    using ServiceList = std::list<std::unique_ptr<Service>>;
    ServiceList _services;
    log::Logger _log;
};


}
