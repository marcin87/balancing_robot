#pragma once

#include "service.hpp"
#include <memory>
#include <string>
#include <chrono>


namespace service {

class ServiceFactory
{
public:
    static std::unique_ptr<Service> createService(const std::string& name,
					   ServiceId address,
					   std::chrono::milliseconds msgWaitTime);

};

}
