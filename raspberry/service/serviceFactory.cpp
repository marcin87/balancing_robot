#include "serviceFactory.hpp"
#include "client/client.hpp"
#include "connection/connection.hpp"
#include "logger/logWriter.hpp"
#include "server/server.hpp"
#include "cli/cli.hpp"
#include "exception/robotException.hpp"
#include "testService.hpp"
#include "spicom/spicom.hpp"

namespace service {

std::unique_ptr<Service> ServiceFactory::createService(const std::string& name,
						       ServiceId address,
						       std::chrono::milliseconds msgWaitTime)
{
    if(name == "client")
    {
	return std::make_unique<client::Client>(name, address, msgWaitTime);
    }
    else if(name == "logger")
    {
	return std::make_unique<log::LogWriter>(name, address, msgWaitTime);
    }
    else if(name == "server") 
    {
	return std::make_unique<server::Server>(name, address, msgWaitTime);
    }
    else if(name == "testclient") 
    {
	return std::make_unique<test::TestClient>(name, address, msgWaitTime);
    }
    else if(name == "testserver") 
    {
	return std::make_unique<test::TestServer>(name, address, msgWaitTime);
    }
    else if(name == "cli") 
    {
	return std::make_unique<cli::Cli>(name, address, msgWaitTime);
    }
#ifdef SERVER
    else if(name == "spicom") 
    {
	return std::make_unique<spicom::Spicom>(name, address, msgWaitTime);
    }
#endif
    else
    {
	throw exception::RobotException("Could not create a service. Unrecognized name " + name);
    }

    return nullptr;
}

}
