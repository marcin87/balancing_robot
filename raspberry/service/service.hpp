#pragma once

#include <memory>
#include <thread>
#include <string>
#include <functional>
#include <unordered_map>
#include "msgReceiver.hpp"
#include <atomic>

namespace message {
    class Msg;
}

namespace service {



using ServiceId = unsigned;

class Service
{
public:
    Service(const std::string& name,
	    ServiceId serviceId,
	    std::chrono::milliseconds msgWaitTime = std::chrono::milliseconds(0)); 
    virtual ~Service();

    void start();
    void stop();
    bool isRunning() const noexcept { return _isRunning.load(); }
    std::string getName() const noexcept { return _name; }
    unsigned getId() const noexcept { return _serviceId; }
protected:
    using MsgId = int;
    using MsgHandler = std::function<void(message::Msg&)>;
    using MsgHandlers = std::unordered_map<MsgId, MsgHandler>;

    void run();
    void wait();
    void handleMessage(message::Msg& msg);
    void registerMsgHandler(MsgId msgId, MsgHandler handler);

    virtual void doRun() = 0;
    virtual void doStart() { }
    virtual void doStop() { }
    virtual void handleUnregisteredMsg(std::unique_ptr<message::Msg> msg);

    const std::string _name;
    const unsigned _serviceId;
    std::unique_ptr<std::thread> _thread;
    std::atomic<bool> _isRunning;
    message::MsgReceiver _receiver;
    MsgHandlers _msgHandlers;
};

}
