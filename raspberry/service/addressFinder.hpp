#pragma once
#include <string>

namespace message {
class Address;
}

namespace service {

bool findServiceAddress(const std::string& name, message::Address& address);

}
