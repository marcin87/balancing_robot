#include "serviceManager.hpp"
#include "serviceFactory.hpp"
#include "config/configAccess.hpp"
#include <string>
#include <vector>
#include <iostream>
namespace {

class ServiceInitInfo
{
public:
    ServiceInitInfo(const std::string& name, unsigned address, unsigned msgWaitTime) :
	_name(name),
	_address(address),
	_msgWaitTime(msgWaitTime)
    {
    
    }

    auto getName() const { return _name; }
    auto getAddress() const { return _address; }
    auto getMsgWaitTime() const { return _msgWaitTime; }
private:
    const std::string _name;
    const unsigned _address;
    const unsigned _msgWaitTime;
};


auto readServiceInitInfos()
{
    std::vector<ServiceInitInfo> result;
    auto conf = config::ConfigAccess().get();
    auto services =  conf->services();

    for(const auto& service : *services)
	result.emplace_back(std::string(service->name()->begin(), service->name()->end()),
		    service->address(), service->msgWaitTime());

    return std::move(result);
}

}


namespace service {

ServiceManager::ServiceManager() : 
    _log("ServiceManager")
{
    auto serviceInfos = readServiceInitInfos();
    
    for(const auto& serviceInfo : serviceInfos)
    {
	auto name = serviceInfo.getName();
	auto address= serviceInfo.getAddress();
	auto msgWaitTime = serviceInfo.getMsgWaitTime();

	_log.info("Creating service name=\"%s\", address=%u, msgWaitTime=%u",
		name.c_str(), address, msgWaitTime);
	auto service = ServiceFactory::createService(name,
						    address, 
						    std::chrono::milliseconds(msgWaitTime));
	_services.push_back(std::move(service));
    }
}

ServiceManager::~ServiceManager()
{
    stop(); 
}

void ServiceManager::start()
{
    for(const auto& service : _services)
    {
	if(!service->isRunning())
	{
	    _log.info("Starting service %s", service->getName().c_str());
	    service->start();
	}
    }
}

void ServiceManager::stop()
{
    for(const auto& service : _services)
    {
	if(service->isRunning())
	{
	    _log.info("Stoping service %s", service->getName().c_str());
	    service->stop();
	}
    }
}

void ServiceManager::cleanup()
{
    auto serviceIt = _services.begin();

    while(serviceIt != _services.end())
    {
	auto& service = *serviceIt;

	if(!service->isRunning())
	{
	    _log.info("Removing idle service %s", service->getName().c_str());
	    _services.erase(serviceIt++);
	}
	else
	{
	    ++serviceIt;
	}
    }
}


unsigned ServiceManager::serviceCount() const
{
    return _services.size();
}

}
