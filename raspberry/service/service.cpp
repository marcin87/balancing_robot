#include "service.hpp"
#include <unordered_set>
#include "logger.hpp"
#include "robotException.hpp"

namespace {

class ServiceRegistrationException : public exception::RobotException
{
public:
    ServiceRegistrationException(const std::string& message) :
	RobotException("[Service] " + message)
    {
    
    }

};

using ServiceName = std::string;

static std::unordered_map<ServiceName, service::ServiceId> serviceNameIdMap;
static std::unordered_set<service::ServiceId> registeredServiceIds;

void registerService(const ServiceName& name, service::ServiceId serviceId)
{
    if(registeredServiceIds.count(serviceId))
	throw ServiceRegistrationException("Cannot register the service. Service with id " + 
		std::to_string(serviceId) + " has already been registered");

    if(serviceNameIdMap.find(name) != serviceNameIdMap.end())
	throw ServiceRegistrationException("Cannot register the service. Service with name \"" + 
		name + "\" has already been registered");

    serviceNameIdMap.emplace(name, serviceId);
}

void deregisterService(const ServiceName& name)
{
    registeredServiceIds.erase(serviceNameIdMap.at(name));
    serviceNameIdMap.erase(name);
}

}


namespace service {

bool findServiceAddress(const std::string& name, message::Address& address)
{
    auto found = serviceNameIdMap.find(name);

    if(found == serviceNameIdMap.end())
	return false;

    address.queueId = found->second;

    return true;
}

Service::Service(const std::string& name, unsigned serviceId, std::chrono::milliseconds msgWaitTime) : 
    _name(name),
    _serviceId(serviceId),
    _isRunning(false),
    _receiver(serviceId, msgWaitTime)
{
    registerService(name, serviceId);     
}

Service::~Service()
{
    stop();
    deregisterService(_name);
}

void Service::start()
{
    if(_isRunning)
	return;
    
    doStart();

    _isRunning.store(true);
    _thread = std::make_unique<std::thread>(&Service::run, this);
}

void Service::stop()
{
    bool prevRunning = _isRunning;
    _isRunning.store(false);

    if(_thread && std::this_thread::get_id() != _thread->get_id())
	wait();

    if(!prevRunning)
	return;

    doStop();
}

void Service::run()
{
    while(_isRunning)
    {
	try
	{
	    auto msgPtr = _receiver.receive();

	    if(msgPtr)
		handleMessage(*msgPtr);

	    doRun();
	}
	catch(const exception::RobotException& exception)
	{
	    log::Logger(getName()).error("Unhandled exception: \"%s\". Stopping service \"%s\"",
		    exception.what(), getName().c_str());	
	    stop();
	}
    }
}

void Service::wait()
{
    if(!_thread)
	return;

    if(_thread->joinable())
    {
	_thread->join();
	_thread.reset();
    }
}

void Service::handleMessage(message::Msg& msg)
{
    auto msgId = msg.getMsgId();
    auto found = _msgHandlers.find(msgId);
    
    if(found != _msgHandlers.end())
    {
	found->second(msg);
    }
    else
    {
	handleUnregisteredMsg(msg.move());
    }
}

void Service::registerMsgHandler(MsgId msgId, MsgHandler handler)
{
    _msgHandlers[msgId] = handler;
}

void Service::handleUnregisteredMsg(std::unique_ptr<message::Msg> msg)
{
    log::Logger(getName()).error("Received unrecognized message (msgId=%d)", msg->getMsgId());	
}

}
