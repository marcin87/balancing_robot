

function(set_source_files SRC)
    set(${SRC}
	"main.cpp"
	"service/service.cpp"
	"service/serviceManager.cpp"
	"service/serviceFactory.cpp"
	"logger/logger.cpp"
	"logger/logSender.cpp"
	"logger/logWriter.cpp"
	"config/configAccess.cpp"
	"connection/connection.cpp"
	"connection/msgRouter.cpp"
	"server/server.cpp"
	"message/msgQueue.cpp"
	"message/msgSystem.cpp"
	"message/msgSender.cpp"
	"message/msgReceiver.cpp"
	"utils/fdPoller.cpp"
	"utils/fdReader.cpp"
	"utils/fdWriter.cpp"
	"utils/socketReader.cpp"
	"client/client.cpp"
	"testService.cpp"
	"cli/cli.cpp"
	"cli/commandFactory.cpp"
	"cli/commands/getFreeHeapCmd.cpp"
	"cli/commands/getLogsCmd.cpp"
    PARENT_SCOPE)
endfunction()
