#include "testService.hpp"
#include <cstring>
#include <thread>
#include <chrono>

using namespace std::literals::chrono_literals;

namespace test {

TestClient::TestClient(const std::string& name,
	      service::ServiceId serviceId,
	      std::chrono::milliseconds msgWaitTime) :
    Service(name, serviceId, msgWaitTime),
    _log(name),
    _sender(serviceId)
{
    registerMsgHandler(GET_LOGS_MSG_RSP, [&](message::Msg& msg){
	    auto& payload = msg.getPayload<GetLogsMsgRsp>();
	    _log.info("Received GetLogsMsgRsp");
	    _log.info("isMaster=%d", payload.isMaster);
	    _log.info("lineNum=%d", payload.lineNum);
	    _log.info("totalLineNum=%d", payload.totalLineNum);
	    _log.info("timestamp=%d", payload.timestamp);
	    _log.info("logLevel=%d", payload.logLevel);
	    _log.info("component=%d", payload.component);
	    _log.info("strBuffer=%s", payload.strBuffer);
	    _log.info("status=%d", payload.status);

	    std::this_thread::sleep_for(5s);
	    sendTestMsg();
	    });

}


void TestClient::doRun()
{
    static bool isFirstTime = true;

    if(isFirstTime)
    {
	if(!service::findServiceAddress("client", _clientServAddr)) {
	    _log.error("Could not find client address");
	    return;
	}
	
	isFirstTime = false;
	// sendTestMsg();
    }

}

void TestClient::sendTestMsg()
{
    message::DataMsg<GetLogsMsgReq> msg(INIT_GET_LOGS_MSG_REQ);
    msg.getPayload().isMaster = 1;

    _sender.send(std::move(msg), _clientServAddr);

}


TestServer::TestServer(const std::string& name,
	      service::ServiceId serviceId,
	      std::chrono::milliseconds msgWaitTime) :
    Service(name, serviceId, msgWaitTime),
    _log(name),
    _sender(serviceId)
{
    registerMsgHandler(GET_LOGS_MSG_REQ, [&](message::Msg& msg){
	    auto& payload = msg.getPayload<GetLogsMsgReq>();
	    _log.info("Received GetLogsMsgReq: isMaster=%u", payload.isMaster);
	
	    message::DataMsg<GetLogsMsgRsp> response(INIT_GET_LOGS_MSG_RSP);
	    response.getPayload().isMaster = payload.isMaster;
	    response.getPayload().lineNum = 123;
	    response.getPayload().totalLineNum = 45;
	    response.getPayload().timestamp = 123456;
	    response.getPayload().logLevel = 2;
	    response.getPayload().component = 4;
	    strcpy(reinterpret_cast<char*>(response.getPayload().strBuffer), "Jeden, dwa, trzy...");
	    response.getPayload().status = 1;
	    message::Address address{ msg.getSender() };    
	    _sender.send(std::move(response), address);

	    });

    registerMsgHandler(GET_FREE_HEAP_SIZE_MSG_REQ, [&](message::Msg& msg){
	    auto& payload = msg.getPayload<GetFreeHeapSizeReq>();
	    _log.info("Received GetFreeHeapSizeReq: isMaster=%u", payload.isMaster);
	
	    message::DataMsg<GetFreeHeapSizeRsp> response(INIT_GET_FREE_HEAP_SIZE_MSG_RSP);
	    response.getPayload().heapSize = 1024;
	    message::Address address{ msg.getSender() };    
	    _sender.send(std::move(response), address);

	    });

}


void TestServer::doRun()
{


}

}
