#include "client.hpp"
#include "config/configAccess.hpp"
#include "connection/connection.hpp"
#include "exception/robotException.hpp"
#include "service/addressFinder.hpp"
#include "utils/socketReader.hpp"
#include "serdes/deserializer.hpp"
#include "messages.h"
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <algorithm>

namespace client{

namespace {

constexpr int SOCKET_WAIT_TIME_SEC = 1;

class ClientException : public exception::RobotException
{
public:
    ClientException(const std::string& message) :
	RobotException("[Client] " + message)
    {
    
    }

};

inline unsigned readPortFromConf()
{
    auto conf = config::ConfigAccess().get();
    return conf->client()->port();
}

inline auto readAddressFromConf()
{
    auto conf = config::ConfigAccess().get();
    auto addr = conf->client()->address();
    return std::string(addr->begin(), addr->end());
}

auto translateBufferToMsg(std::vector<char>& buffer)
{
    serialization::Deserializer deserializer(reinterpret_cast<unsigned char*>(buffer.data()));
    return deserializer.deserialize();
}

} // anonymous namespace


class MessageHandler
{
public:
    explicit MessageHandler(Client& client) :
	_client(client),
	_sender(client._msgSender),
	_log(client._log)
    {
	registerMsgHandlers();
    }

private:
    void registerMsgHandlers();
    void handleConnectToRobot(message::Msg& msg);
    void handleClientSetIpAdd(message::Msg& msg);
    void handleClientSetPort(message::Msg& msg);

    static constexpr unsigned char RESP_STATUS_OK = 1;
    static constexpr unsigned char RESP_STATUS_NOK = 0;
    Client& _client;
    message::MsgSender& _sender;
    log::Logger& _log;
};

void MessageHandler::registerMsgHandlers()
{
    _client.registerMsgHandler(CLIENT_CONNECT_TO_ROBOT_MSG_REQ, [&](auto& msg){
		handleConnectToRobot(msg);
	    });

    _client.registerMsgHandler(CLIENT_SET_IP_ADDR_MSG_REQ, [&](auto& msg){
		handleClientSetIpAdd(msg);
	    });

    _client.registerMsgHandler(CLIENT_SET_PORT_MSG_REQ, [&](auto& msg){
		handleClientSetPort(msg);
	    });

}

void MessageHandler::handleConnectToRobot(message::Msg& msg)
{
    auto status = RESP_STATUS_OK;
    _log.info("Received ClientConnectToRobotMsgReq request");

    auto& payload = msg.getPayload<ClientConnectToRobotMsgReq>();

    try
    {
	payload.isConnected ? _client.connect() : _client.disconnect();	
    }
    catch (const exception::RobotException& exception)
    {
	_log.error("Exception: %s", exception.what());
	status = RESP_STATUS_NOK;
    }

    message::DataMsg<ClientConnectToRobotMsgRsp> response(INIT_CLIENT_CONNECT_TO_ROBOT_MSG_RSP);
    response.getPayload().status = status;
    message::Address address{ msg.getSender() };    
    _sender.send(std::move(response), address);
}

void MessageHandler::handleClientSetIpAdd(message::Msg& msg)
{
    _log.info("Received ClientSetIpAddrMsgReq request");
    auto& payload = msg.getPayload<ClientSetIpAddrMsgReq>();
    _client.setAddress(reinterpret_cast<char*>(payload.address));	

    message::DataMsg<ClientSetIpAddrMsgRsp> response(INIT_CLIENT_SET_IP_ADDR_MSG_RSP);
    response.getPayload().status = RESP_STATUS_OK;
    message::Address address{ msg.getSender() };    
    _sender.send(std::move(response), address);
}

void MessageHandler::handleClientSetPort(message::Msg& msg)
{
    _log.info("Received ClientSetPortMsgReq request");
    auto& payload = msg.getPayload<ClientSetPortMsgReq>();
    _client.setPort(payload.port);	

    message::DataMsg<ClientSetPortMsgRsp> response(INIT_CLIENT_SET_PORT_MSG_RSP);
    response.getPayload().status = RESP_STATUS_OK;
    message::Address address{ msg.getSender() };    
    _sender.send(std::move(response), address);
}

Client::Client(const std::string& name, service::ServiceId serviceId, std::chrono::milliseconds msgWaitTime) :
    service::Service(name, serviceId, msgWaitTime),
    _ipAddress(readAddressFromConf()),
    _port(static_cast<int>(readPortFromConf())),
    _sockFd(-1),
    _fdPoller(utils::FdPoller::PollType::READ, std::chrono::seconds(SOCKET_WAIT_TIME_SEC)),
    _connected(false),
    _connectionId(0),
    _log(name),
    _msgSender(getId()),
    _msgHandler(std::make_unique<MessageHandler>(*this))
{
}

Client::~Client() = default;


void Client::doRun()
{
    if(!_connected)
	connect();

    if(_fdPoller.poll())
    {
	if(!_fdPoller.getFailedFds().empty())
	    handleSocketFaults();

	if(!_fdPoller.getReadyFds().empty())
	    handleSocketMessage();
    }
    else
    {
	if(!_connection->isRunning())
	    removeConnection();
    }

}

void Client::doStart()
{
    _log.info("Starting client");
}

void Client::doStop()
{
    _log.info("Stopping client");

    if(_connected)
	disconnect();
}

void Client::connect()
{
    _log.info("Connecting to server...");

    if(_connected)
	disconnect();

    sockaddr_in serverAddr {};
    _sockFd = socket(AF_INET, SOCK_STREAM, 0);
   
    if (_sockFd < 0)
	throw ClientException("Failed to open a socket");

    _log.debug("Opened socket");
    auto plainAddr = inet_addr(_ipAddress.c_str());
    
    if(plainAddr == -1)
    {
	auto serverHostent = gethostbyname(_ipAddress.c_str());

	if (!serverHostent)
	{
	    close(_sockFd);
	    throw ClientException("Could not find a host with name=" + _ipAddress);
	}

	auto addressList = reinterpret_cast<in_addr**>(serverHostent->h_addr_list);

	if(!addressList[0])
	{
	    close(_sockFd);
	    throw ClientException("Could not resolve address. Empty address list");
	}
	serverAddr.sin_addr = *addressList[0];

	_log.info("Host address resolved using name=%s to address=%s",
		_ipAddress.c_str(), inet_ntoa(*addressList[0]));
    }
    else
    {
	serverAddr.sin_addr.s_addr = plainAddr;
	_log.info("Host address resolved using plain address=%s", _ipAddress.c_str());
    }

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(_port);

    if(::connect(_sockFd, reinterpret_cast<sockaddr*>(&serverAddr), sizeof(serverAddr)) < 0)
    {
        close(_sockFd);
	throw ClientException("Could not connect to remote host");
    }
    
    _connected = true;
    _fdPoller.addFd(_sockFd);
    createConnection(_sockFd);

    _log.info("Successfully connected to remote host(%s:%d)", _ipAddress.c_str(), _port);
}

void Client::disconnect()
{
    _log.info("Disconnecting from server");
    
    removeConnection();
}

void Client::createConnection(int sockFd)
{
    using namespace std::chrono_literals;
    _log.info("Creating a new connection from sockFd=%d", sockFd);
    
    const std::string connectionName("connection");
    _connection = std::make_unique<connection::Connection>(connectionName,
						      _connectionId++,
						      1ms,
						      sockFd);
    _connection->start();
    _log.debug("New connection \"%s\" started", connectionName.c_str());

    _fdPoller.addFd(sockFd);
    
    if(!service::findServiceAddress(connectionName, _connectionAddres))
	throw ClientException("Could not find address for connection \"" + connectionName + "\"");

}

void Client::removeConnection()
{
    _log.info("Removing connection \"%s\"", _connection->getName().c_str());

    if(_connection->isRunning())
	_connection->stop();
    
    _connected = false;
    ::close(_sockFd);
    _fdPoller.removeFd(_sockFd);
    _connection.reset();

    _log.debug("Connection removed");
}

void Client::setAddress(const std::string& ipAddress)
{
    _ipAddress = ipAddress;
    _log.info("Remote host IP address has been set to %s", ipAddress.c_str());
}

void Client::setPort(unsigned port)
{
    _port = port;
    _log.info("Port has been set to %d", port);
}

void Client::handleSocketFaults()
{
    auto faults = _fdPoller.getFdFaults(_sockFd);

    if(faults.empty())
	return;

    std::string faultsStr;
    const std::string separator(", ");
    std::for_each(faults.cbegin(), faults.cend(),
	    [&](const auto& fault){ faultsStr += fdFaultToStr(fault) + separator; });

    auto lastSep = faultsStr.rfind(separator);

    if(lastSep != std::string::npos)
	faultsStr.erase(lastSep, faultsStr.length());

    _log.error("Faults %s has been raised for socket %d. Closing connection",
	    faultsStr.c_str(), _sockFd);

    removeConnection();
}

void Client::handleSocketMessage()
{
    try
    {
	auto msgBuffer(std::move(utils::readSocketMsgBuffer(_sockFd)));

	if(msgBuffer.empty())
	{
	    _log.info("Server socket closed. Removing connection.");
	    removeConnection();
	    return;
	}

	auto msg(translateBufferToMsg(msgBuffer));
	dispatchMessage(std::move(msg));
    }
    catch(const exception::RobotException& exception)
    {
	_log.error("Exception: \"%s\"", exception.what());
    }
}

void Client::dispatchMessage(std::unique_ptr<message::Msg> msg)
{
    try
    {
	_msgSender.forward(std::move(msg),  _connectionAddres);
    }
    catch(const exception::RobotException& exception)
    {
	_log.error("Could not forward the message (id=%d) to \"%s\". Exception: \"%s\"",
		msg->getMsgId(), _connection->getName().c_str(), exception.what());
    }
}



}
