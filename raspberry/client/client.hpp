#pragma once

#include "service/service.hpp"
#include "logger/logger.hpp"
#include "utils/fdPoller.hpp"
#include "message/msgSender.hpp"
#include <string>
#include <memory>

namespace connection {
class Connection;
}

namespace client {

class MessageHandler;

class Client : public service::Service
{
    friend class MessageHandler;
public:
    Client(const std::string& name,
	   service::ServiceId serviceId,
	   std::chrono::milliseconds msgWaitTime = std::chrono::milliseconds(0));
    ~Client();
protected:

    virtual void doRun() override;
    virtual void doStart() override;
    virtual void doStop() override;
    
    void connect();
    void disconnect();
    void createConnection(int sockFd);
    void removeConnection();
    void setAddress(const std::string& ipAddress);
    void setPort(unsigned port);
    void handleSocketFaults();
    void handleSocketMessage();
    void dispatchMessage(std::unique_ptr<message::Msg> msg);

    std::string _ipAddress;
    int _port;
    int _sockFd;
    utils::FdPoller _fdPoller;
    bool _connected;
    service::ServiceId _connectionId;
    std::unique_ptr<connection::Connection> _connection;
    message::Address _connectionAddres;
    log::Logger _log;
    message::MsgSender _msgSender; 
    std::unique_ptr<MessageHandler> _msgHandler;
};



}
