#include "config/config_generated.h"

namespace config {

class ConfigAccess
{
public:

    const Config* get();
};


}
