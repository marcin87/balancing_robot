#include "configAccess.hpp"
#include <fstream>
#include <algorithm>
#include <vector>
#include <exception>
#include <iterator>

namespace config {
    
namespace {

#ifdef CLIENT
static const std::string fileName("configClient.bin");
#else
static const std::string fileName("configServer.bin");
#endif

auto readConfig()
{
    static std::vector<char> buffer;
    std::ifstream file(fileName, std::ios::binary);

    if(!file.is_open())
	throw std::runtime_error("Error opening " + fileName);

    buffer.clear();
    std::copy(std::istreambuf_iterator<char>(file), 
	      std::istreambuf_iterator<char>(),
	      std::back_inserter(buffer));

    return GetConfig(buffer.data());
}

}

const Config* ConfigAccess::get()
{
    static auto config = readConfig();

    return config;
}



}
