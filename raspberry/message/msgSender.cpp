#include "msgSender.hpp"

namespace message {

MsgSender::MsgSender(MsgQueueId queueId) :
    _queueId(queueId),
    _msgSystem(MsgSystem::instance())
{
    _msgSystem.registerQueue(_queueId);
}

MsgSender::~MsgSender()
{
    _msgSystem.deregisterQueue(_queueId);
}

void MsgSender::send(Msg&& msg, Address receiver)
{
    _msgSystem.push(std::move(msg), receiver.queueId, _queueId); 
}

void MsgSender::send(std::unique_ptr<Msg> msg, Address receiver)
{
    _msgSystem.push(std::move(msg), receiver.queueId, _queueId); 
}

void MsgSender::forward(Msg&& msg, Address receiver)
{
    _msgSystem.push(std::move(msg), receiver.queueId); 
}

void MsgSender::forward(std::unique_ptr<Msg> msg, Address receiver)
{
    _msgSystem.push(std::move(msg), receiver.queueId); 
}

}
