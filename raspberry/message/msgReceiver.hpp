#pragma once

#include "msgSystem.hpp"
#include "address.hpp"

namespace message {

class Msg;

class MsgReceiver
{
public:
    MsgReceiver(MsgQueueId queueId, std::chrono::milliseconds timeout = std::chrono::milliseconds(0));
    ~MsgReceiver();

    MsgReceiver(MsgReceiver&) = delete;
    MsgReceiver& operator=(MsgReceiver) = delete;
    
    std::unique_ptr<Msg> receive();
    void setTimeout(std::chrono::seconds timeout);
private:
    MsgQueueId _queueId;
    MsgSystem& _msgSystem;
    std::chrono::milliseconds _timeout;
};


}
