#pragma once

#include "msgSystem.hpp"
#include "address.hpp"
#include <memory>

namespace message {

class Msg;

class MsgSender
{
public:
    MsgSender(MsgQueueId queueId);
    ~MsgSender();

    MsgSender(MsgSender&) = delete;
    MsgSender& operator=(MsgSender) = delete;

    void send(Msg&& msg, Address receiver);
    void send(std::unique_ptr<Msg> msg, Address receiver);
    void forward(Msg&& msg, Address receiver);
    void forward(std::unique_ptr<Msg> msg, Address receiver);
private:
    MsgQueueId _queueId;
    MsgSystem& _msgSystem;
};


}
