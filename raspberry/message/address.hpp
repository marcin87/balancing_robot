#pragma once

#include "msgSystem.hpp"

namespace message {

struct Address
{
    MsgQueueId queueId;
};

}
