#include "msgSystem.hpp"
#include <stdexcept>
#include <string>


namespace message {

MsgSystem& MsgSystem::instance()
{
    static MsgSystem msgSystem;

    return msgSystem;
}

void MsgSystem::push(Msg&& msg, MsgQueueId receiver, MsgQueueId sender)
{
    msg.setSender(sender);
    push(std::move(msg), receiver);
}

void MsgSystem::push(std::unique_ptr<Msg> msg, MsgQueueId receiver, MsgQueueId sender)
{
    msg->setSender(sender);
    push(std::move(msg), receiver);
}

void MsgSystem::registerQueue(MsgQueueId queueId)
{
    std::lock_guard<std::mutex> lock(_mutex);

    if(isQueueRegistered(queueId))
    {
	++_queuesMap.at(queueId)->refCount;
	return;
    }

    auto queueKeeper = std::make_unique<MsgQueueKeeper>();
    queueKeeper->refCount = 1;
    _queuesMap.emplace(queueId, std::move(queueKeeper));
}

void MsgSystem::deregisterQueue(MsgQueueId queueId)
{
    std::lock_guard<std::mutex> lock(_mutex);

    if(!isQueueRegistered(queueId))
	throw MsgSystemException("Can't deregister a queue id " + 
		std::to_string(queueId) + ". The queue is not registered");
    
    --_queuesMap.at(queueId)->refCount;

    if(!_queuesMap.at(queueId)->refCount)
	_queuesMap.erase(queueId);
}

std::unique_ptr<Msg> MsgSystem::dispatch(MsgQueueId queueId, std::chrono::milliseconds timeout)
{
    std::unique_lock<std::mutex> lock(_mutex);

    if(!isQueueRegistered(queueId))
	throw MsgSystemException("Can't dispatch the message. Chosen queue id " + 
		std::to_string(queueId) + " has not been registered");

    if(_queuesMap.at(queueId)->queue.empty() && 
       _queuesMap.at(queueId)->cv.wait_for(lock, timeout) == std::cv_status::timeout)
    {
	return nullptr;
    }

    return _queuesMap.at(queueId)->queue.pop();
}

bool MsgSystem::isQueueRegistered(MsgQueueId queueId)
{
    return _queuesMap.find(queueId) != _queuesMap.end();
}

}
