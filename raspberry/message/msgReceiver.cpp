#include "msgReceiver.hpp"

namespace message {

MsgReceiver::MsgReceiver(MsgQueueId queueId, std::chrono::milliseconds timeout) :
    _queueId(queueId),
    _msgSystem(MsgSystem::instance()),
    _timeout(timeout)
{
    _msgSystem.registerQueue(_queueId);
}

MsgReceiver::~MsgReceiver()
{
    _msgSystem.deregisterQueue(_queueId);
}

std::unique_ptr<Msg> MsgReceiver::receive()
{
    return _msgSystem.dispatch(_queueId, _timeout);
}

void MsgReceiver::setTimeout(std::chrono::seconds timeout)
{
    _timeout = timeout;    
}

}
