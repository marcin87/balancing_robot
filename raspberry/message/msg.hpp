#pragma once
#include <memory>

namespace message {

template <typename T>
class DataMsg;

class Msg
{
public:
    Msg(int msgId, unsigned payloadSize = 0, const char* payloadBuffer = nullptr) : 
	_msgId(msgId),
	_sender(0),
	_placeholder(0),
	_payloadSize(payloadSize),
	_payloadRawBuffer(payloadBuffer)
    {
    }


    Msg(Msg&&) = default;
    Msg& operator=(Msg&&) = default;

    Msg(const Msg&) = delete;
    Msg& operator=(const Msg&) = delete;

    virtual std::unique_ptr<Msg> move()
    {
	return std::make_unique<Msg>(std::move(*this));
    }

    virtual ~Msg() = default;

    int getMsgId() const
    {
	return _msgId;
    }

    void setSender(unsigned sender)
    {
	_sender = sender;
    }

    unsigned getSender() const
    {
	return _sender;
    }

    void setPlaceholder(unsigned placeholder)
    {
	_placeholder = placeholder;
    }

    unsigned getPlaceholder() const
    {
	return _placeholder;
    }

    unsigned getPayloadSize() const
    {
	return _payloadSize;
    }

    const char* getPayloadRawBuffer()
    {
	return _payloadRawBuffer;
    }

    template <typename PayloadType>
    PayloadType& getPayload()
    {
	return 	static_cast<DataMsg<PayloadType>&>(*this).getPayload();
    }

protected:
    void setPayloadBuffer(const char* buffer)
    {
	_payloadRawBuffer = buffer;
    }

private:
    const int _msgId;
    unsigned _sender;
    unsigned _placeholder;
    const unsigned _payloadSize;
    const char* _payloadRawBuffer;
};

template <typename PayloadType>
class DataMsg : public Msg
{
public:
    DataMsg(int msgId, PayloadType& payload):
	Msg(msgId, sizeof(PayloadType)),
        _payload(std::make_unique<PayloadType>(payload))
    {
	setPayloadBuffer(reinterpret_cast<const char*>(_payload.get()));
    }

    DataMsg(const PayloadType& payload):
	Msg(payload.header.msgId, sizeof(PayloadType)),
        _payload(std::make_unique<PayloadType>(payload))
    {
	setSender(payload.header.queueId);
	setPlaceholder(payload.header.slot);
	setPayloadBuffer(reinterpret_cast<const char*>(_payload.get()));
    }

    DataMsg(int msgId, std::unique_ptr<PayloadType> payload):
	Msg(msgId, sizeof(PayloadType), reinterpret_cast<const char*>(payload.get())),
        _payload(std::move(payload))
    {
    }

    DataMsg(std::unique_ptr<PayloadType> payload):
	Msg(payload->header.msgId, sizeof(PayloadType), reinterpret_cast<const char*>(payload.get())),
        _payload(std::move(payload))
    {
	setSender(_payload->header.queueId);
	setPlaceholder(_payload->header.slot);
    }


    DataMsg(DataMsg&&) = default;
    DataMsg& operator=(DataMsg&&) = default;

    DataMsg(const DataMsg&) = delete;
    DataMsg& operator=(const DataMsg&) = delete;

    virtual std::unique_ptr<Msg> move() override
    {
	return std::make_unique<DataMsg<PayloadType>>(std::move(*this));
    }

    virtual ~DataMsg() = default;

    PayloadType& getPayload()
    {
	return *_payload;
    }

private:
    std::unique_ptr<PayloadType> _payload;
};


}
