#include "msgQueue.hpp"

namespace message {

void MsgQueue::push(Msg&& msg)
{
    _queue.push(msg.move());
}

void MsgQueue::push(std::unique_ptr<Msg> msg)
{
    _queue.push(std::move(msg));
}

std::unique_ptr<Msg> MsgQueue::pop()
{
    auto msg = _queue.front()->move();
    _queue.pop();
    return msg;
}

bool MsgQueue::empty() const
{
    return _queue.empty();
}

}
