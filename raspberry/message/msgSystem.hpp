#pragma once

#include "msgQueue.hpp"
#include "exception/robotException.hpp"
#include <unordered_map>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <chrono>

namespace message {

class MsgSystemException : public exception::RobotException
{
public:
    MsgSystemException(const std::string& message) :
	RobotException("[MsgSystem] " + message)
    {
    
    }

};


class Msg;

using MsgQueueId = unsigned;

class MsgSystem
{
    template <typename MsgType>
    friend void push(MsgType&&, MsgQueueId);
public:
    MsgSystem(const MsgSystem&) = delete;
    MsgSystem& operator=(MsgSystem) = delete;

    static MsgSystem& instance();

    template<typename MsgType>
    void push(MsgType&& msg, MsgQueueId receiver)
    {
	std::unique_lock<std::mutex> lock(_mutex);

	if(!isQueueRegistered(receiver))
	    throw MsgSystemException("Can't push the message. Chosen receiver " 
		    + std::to_string(receiver) + " has not been registered");
	
	_queuesMap.at(receiver)->queue.push(std::forward<MsgType>(msg));
	_queuesMap.at(receiver)->cv.notify_one();
    }

    void push(Msg&& msg, MsgQueueId receiver, MsgQueueId sender);
    void push(std::unique_ptr<Msg> msg, MsgQueueId receiver, MsgQueueId sender);
    void registerQueue(MsgQueueId queueId);
    void deregisterQueue(MsgQueueId queueId);
    std::unique_ptr<Msg> dispatch(MsgQueueId queueId, std::chrono::milliseconds timeout = std::chrono::milliseconds(0));
private:
    MsgSystem() { }

    bool isQueueRegistered(MsgQueueId queueId);

    struct MsgQueueKeeper
    {
	MsgQueueKeeper() : refCount(0) { }

	MsgQueue queue;
	std::condition_variable cv;
	unsigned refCount;
    };

    using MsgQueueKeeperPtr = std::unique_ptr<MsgQueueKeeper>;

    std::unordered_map<MsgQueueId, MsgQueueKeeperPtr> _queuesMap;
    std::mutex _mutex;
};

}
