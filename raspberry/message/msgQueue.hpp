#pragma once

#include <queue>
#include <memory>
#include "msg.hpp"

namespace message {

class MsgQueue
{
public:
    void push(Msg&& msg);
    void push(std::unique_ptr<Msg> msg);
    std::unique_ptr<Msg> pop();
    bool empty() const;
private:
    std::queue<std::unique_ptr<Msg>> _queue;
};

}
