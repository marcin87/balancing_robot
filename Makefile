#
# Defines the part type that this project uses.
#
PART=TM4C123GH6PM

ROOT=.
#COMPILER=build

#
# Include the common make definitions.
#
include ${ROOT}/makedefs

#
# Source file directories
#
VPATH=${ROOT}/externals/freertos/FreeRTOS/Source/portable/GCC/ARM_CM4F
VPATH+=${ROOT}/externals/freertos/FreeRTOS/Source/portable/MemMang/
VPATH+=${ROOT}/externals/freertos/FreeRTOS/Source
VPATH+=${ROOT}/cc3100
VPATH+=${ROOT}/cc3100/simplelink
VPATH+=${ROOT}/cc3100/simplelink/source
VPATH+=${ROOT}/cc3100/platform/tiva-c-launchpad
VPATH+=${ROOT}/cc3100/oslib
VPATH+=${ROOT}/utils/
VPATH+=${ROOT}/messages
VPATH+=robot
VPATH+=robot/drivers
VPATH+=robot/drivers/i2c
VPATH+=robot/drivers/spi
VPATH+=robot/drivers/udma
VPATH+=robot/utils
VPATH+=robot/robot
VPATH+=robot/messages
VPATH+=robot/freertos
VPATH+=robot/logger
VPATH+=robot/server
VPATH+=robot/interrupts
VPATH+=robot/updater
VPATH+=robot/config
VPATH+=robot/watchdog

#
# Header file directories
# 
IPATH=.
IPATH+=robot
IPATH+=${ROOT}/common
IPATH+=${ROOT}/externals/freertos/FreeRTOS/Source/portable/GCC/ARM_CM4F
IPATH+=${ROOT}/externals/freertos/FreeRTOS
IPATH+=${ROOT}/externals/freertos/FreeRTOS/Source/include
IPATH+=${ROOT}/externals
IPATH+=${ROOT}/cc3100
IPATH+=${ROOT}/cc3100/simplelink
IPATH+=${ROOT}/cc3100/simplelink/include
IPATH+=${ROOT}/cc3100/platform/tiva-c-launchpad
IPATH+=${ROOT}/cc3100/oslib
IPATH+=${ROOT}/utils/
IPATH+=${ROOT}/messages/
IPATH+=robot/drivers
IPATH+=robot/drivers/i2c
IPATH+=robot/drivers/spi
IPATH+=robot/drivers/udma
IPATH+=robot/utils
IPATH+=robot/messages
IPATH+=robot/robot
IPATH+=robot/freertos
IPATH+=robot/logger
IPATH+=robot/server
IPATH+=robot/interrupts
IPATH+=robot/updater
IPATH+=robot/config
IPATH+=robot/watchdog

DRIVERLIB_DIR=driverlib

.PHONY: driverlib
#
# The default rule for building robot project, should not be used
#
all: ${COMPILER}
all: ${COMPILER}/robot_main.axf

#
# The rule for building the robot project for the master board
#
master: ${COMPILER}
master: ${COMPILER}/robot_main.axf

#
# The rule for building the robot project the for slave board
#
slave: ${COMPILER}
slave: ${COMPILER}/robot_main.axf

#
# The rule to clean out all the build products.
#
clean: ${cleandriverlib}
clean:
	@rm -rf ${COMPILER} ${wildcard *~}

#
# The rule to create the target directory.
#
${COMPILER}:
	@mkdir -p ${COMPILER}



driverlib:
	$(info $$DRIVERLIB_DIR is [${DRIVERLIB_DIR}])
	make -C driverlib || exit $$?;

cleandriverlib:
	make -C driverlib clean;


ifeq ($(MAKECMDGOALS),master)
${COMPILER}/robot_main.axf: ${COMPILER}/I2CWrapper.o
${COMPILER}/robot_main.axf: ${COMPILER}/MCP23017.o
${COMPILER}/robot_main.axf: ${COMPILER}/i2cManager.o
${COMPILER}/robot_main.axf: ${COMPILER}/i2cTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/motor.o
${COMPILER}/robot_main.axf: ${COMPILER}/motorsTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/encoder.o
${COMPILER}/robot_main.axf: ${COMPILER}/encoderSamplerTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/encodersTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/wheelsTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/wheel.o
${COMPILER}/robot_main.axf: ${COMPILER}/led.o
${COMPILER}/robot_main.axf: ${COMPILER}/MPU6050.o
${COMPILER}/robot_main.axf: ${COMPILER}/mpuTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/motionControlTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/motionControlComTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/motionControl.o
${COMPILER}/robot_main.axf: ${COMPILER}/PIDcontroller.o
else ifeq ($(MAKECMDGOALS),slave)
${COMPILER}/robot_main.axf: ${COMPILER}/device.o
${COMPILER}/robot_main.axf: ${COMPILER}/driver.o
${COMPILER}/robot_main.axf: ${COMPILER}/flowcont.o
${COMPILER}/robot_main.axf: ${COMPILER}/fs.o
${COMPILER}/robot_main.axf: ${COMPILER}/board.o
${COMPILER}/robot_main.axf: ${COMPILER}/netapp.o
${COMPILER}/robot_main.axf: ${COMPILER}/netcfg.o
${COMPILER}/robot_main.axf: ${COMPILER}/socket.o
${COMPILER}/robot_main.axf: ${COMPILER}/socket.o
${COMPILER}/robot_main.axf: ${COMPILER}/spawn.o
${COMPILER}/robot_main.axf: ${COMPILER}/osi_freertos.o
${COMPILER}/robot_main.axf: ${COMPILER}/wlan.o
${COMPILER}/robot_main.axf: ${COMPILER}/SimpleLinkWrapper.o
${COMPILER}/robot_main.axf: ${COMPILER}/tcpServer.o
${COMPILER}/robot_main.axf: ${COMPILER}/tcpServerTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/tcpServerHandlerTask.o
else
${COMPILER}/robot_main.axf: ${COMPILER}/device.o
${COMPILER}/robot_main.axf: ${COMPILER}/driver.o
${COMPILER}/robot_main.axf: ${COMPILER}/flowcont.o
${COMPILER}/robot_main.axf: ${COMPILER}/fs.o
${COMPILER}/robot_main.axf: ${COMPILER}/netapp.o
${COMPILER}/robot_main.axf: ${COMPILER}/netcfg.o
${COMPILER}/robot_main.axf: ${COMPILER}/socket.o
${COMPILER}/robot_main.axf: ${COMPILER}/spawn.o
${COMPILER}/robot_main.axf: ${COMPILER}/board.o
${COMPILER}/robot_main.axf: ${COMPILER}/osi_freertos.o
${COMPILER}/robot_main.axf: ${COMPILER}/wlan.o
${COMPILER}/robot_main.axf: ${COMPILER}/I2CWrapper.o
${COMPILER}/robot_main.axf: ${COMPILER}/MCP23017.o
${COMPILER}/robot_main.axf: ${COMPILER}/i2cManager.o
${COMPILER}/robot_main.axf: ${COMPILER}/i2cTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/motor.o
${COMPILER}/robot_main.axf: ${COMPILER}/motorsTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/SimpleLinkWrapper.o
${COMPILER}/robot_main.axf: ${COMPILER}/tcpServer.o
${COMPILER}/robot_main.axf: ${COMPILER}/tcpServerTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/tcpServerHandlerTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/encoder.o
${COMPILER}/robot_main.axf: ${COMPILER}/encoderSamplerTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/encodersTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/wheelsTask.o
endif
${COMPILER}/robot_main.axf: ${COMPILER}/robot_main.o
${COMPILER}/robot_main.axf: ${COMPILER}/robot.o
${COMPILER}/robot_main.axf: ${COMPILER}/spi.o
${COMPILER}/robot_main.axf: ${COMPILER}/uart.o
${COMPILER}/robot_main.axf: ${COMPILER}/uartstdio.o
${COMPILER}/robot_main.axf: ${COMPILER}/spiCom.o
${COMPILER}/robot_main.axf: ${COMPILER}/udmaWrapper.o
${COMPILER}/robot_main.axf: ${COMPILER}/utils.o
${COMPILER}/robot_main.axf: ${COMPILER}/heap_5.o
${COMPILER}/robot_main.axf: ${COMPILER}/memory.o
${COMPILER}/robot_main.axf: ${COMPILER}/list.o
${COMPILER}/robot_main.axf: ${COMPILER}/port.o
${COMPILER}/robot_main.axf: ${COMPILER}/queue.o
${COMPILER}/robot_main.axf: ${COMPILER}/tasks.o
${COMPILER}/robot_main.axf: ${COMPILER}/timers.o
${COMPILER}/robot_main.axf: ${COMPILER}/messages.o
${COMPILER}/robot_main.axf: ${COMPILER}/logger.o
${COMPILER}/robot_main.axf: ${COMPILER}/serverSpiCom.o
${COMPILER}/robot_main.axf: ${COMPILER}/serverSpiComTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/interrupts.o
${COMPILER}/robot_main.axf: ${COMPILER}/msgSystem.o
${COMPILER}/robot_main.axf: ${COMPILER}/circularBuffer.o
${COMPILER}/robot_main.axf: ${COMPILER}/updater.o
${COMPILER}/robot_main.axf: ${COMPILER}/updaterTask.o
${COMPILER}/robot_main.axf: ${COMPILER}/config.o
${COMPILER}/robot_main.axf: ${COMPILER}/wdg.o
${COMPILER}/robot_main.axf: ${ROOT}/startup_${COMPILER}.o
${COMPILER}/robot_main.axf: ${ROOT}/driverlib/${COMPILER}/libdriver.a
${COMPILER}/robot_main.axf: robot_main.ld
SCATTERgcc_robot_main=robot_main.ld
ENTRY_robot_main=ResetISR
CFLAGSgcc=-DTARGET_IS_TM4C123_RB1 -D_USE_RTOS_ALLOCATION -g -O0 -funwind-tables 
ifeq ($(MAKECMDGOALS),master)
CFLAGSgcc+=-D_ROBOT_MASTER_BOARD
endif
#CFLAGSgcc=-DTARGET_IS_TM4C123_RB1 -D_USE_CLI_ -D_DISABLE_I2C_ACK -g

#
# Include the automatically generated dependency files.
#
ifneq (${MAKECMDGOALS},clean)
-include ${wildcard ${COMPILER}/*.d} __dummy__
endif
