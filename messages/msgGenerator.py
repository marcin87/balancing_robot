import sys
import shutil
import os
import glob
from enum import Enum


inDir = ""
outDir = ""

class Event(Enum):
    TYPEDEF         = 1
    BRACE_OPEN      = 2
    STR_LITERAL     = 3
    BRACE_CLOSED    = 4
    EXTERN          = 5
    ENUM            = 6
    START_FLAT_MSG  = 7   
    START_SPI_MSG   = 8   
    NONE            = 9

def eventToStr(event):
    if event == Event.TYPEDEF:
        return "TYPEDEF"
    if event == Event.BRACE_OPEN:
        return "BRACE_OPEN"
    if event == Event.STR_LITERAL:
        return "STR_LITERAL"
    if event == Event.BRACE_CLOSED:
        return "BRACE_CLOSED"
    if event == Event.EXTERN:
        return "EXTERN"
    if event == Event.ENUM:
        return "ENUM"
    if event == Event.START_FLAT_MSG:
        return "START_FLAT_MSG"
    if event == Event.START_SPI_MSG:
        return "START_SPI_MSG"
    if event == Event.NONE:
        return "NONE"

class Type(Enum):
    U8         = 1
    U16        = 2
    U32        = 3
    U64        = 4 
    I8         = 5
    I16        = 6
    I32        = 7
    I64        = 8
    FLOAT      = 9
    DOUBLE     = 10
    BOOL       = 11
    MSGHEADER  = 12

class TypeSpec(Enum):
    PLAIN = 1
    PTR   = 2
    ARRAY = 3

typeStrMap = {
        Type.U8         : "uint8_t",
        Type.U16        : "uint16_t",
        Type.U32        : "uint32_t",
        Type.U64        : "uint64_t",
        Type.I8         : "int8_t",
        Type.I16        : "int16_t",
        Type.I32        : "int32_t",
        Type.I64        : "int64_t",
        Type.FLOAT      : "float",
        Type.DOUBLE     : "double",
        Type.BOOL       : "bool",
        Type.MSGHEADER  : "MsgHeader"
        }



strTypeMap = {
        "uint8_t"  : Type.U8,
        "uint16_t" : Type.U16,
        "uint32_t" : Type.U32,
        "uint64_t" : Type.U64,
        "int8_t"   : Type.I8,
        "int16_t"  : Type.I16,
        "int32_t"  : Type.I32,
        "int64_t"  : Type.I64,
        "float"    : Type.FLOAT,
        "double"   : Type.DOUBLE,
        "bool"     : Type.BOOL,
        "MsgHeader": Type.MSGHEADER
        }

wordToEventMap = {
        "typedef"                         : Event.TYPEDEF,
        "{"                               : Event.BRACE_OPEN,
        "}"                               : Event.BRACE_CLOSED,
        "extern"                          : Event.EXTERN,
        "enum"                            : Event.ENUM,
        "//START_GENERATE_FLATBUFFERS"    : Event.START_FLAT_MSG,
        "//START_GENERATE_SPI_MSG"        : Event.START_SPI_MSG
        }


def strToType(strType):
    return strTypeMap[strType]  

def wordToEvent(word):
    if word in wordToEventMap:
        return wordToEventMap[word]
    
    if len(word) > 0 and isinstance(word, str):
        return Event.STR_LITERAL

    return Event.NONE

class Field:
    def __init__(self, fieldType, name, typeSpec):
        self.type = fieldType
        self.name = name
        self.typeSpec = typeSpec

class Msg:
    def __init__(self):
        self.name = ""
        self.fields = []
        self.idName = ""
        self.initName = ""
        self.isFlatBuffer = False
        self.isSpi = False

    def addField(self, field):
        self.fields.append(field)

    def getFields(self):
        return self.fields

class StateMachine:
    lineCount = 0
    messages = []
    currentMsg = None
    msgIds = []
    msgIdSet = None
    isFlatMsg = False
    isSpi = False
    def __init__(self, state):
        self.currentState = state

    def process(self, line):
        StateMachine.lineCount += 1
        words = [x.strip('\n\t;, ') for x in line.split()]
        event = Event.NONE

        if words:
            if words[0].find('}') != -1:
                words[0] = words[0][1:]
                words.insert(0,'}')
            if words[0].find('*') != -1:
                words[0] = words[0][:-1]

            event = wordToEvent(words[0])
        try:
            self.currentState = self.currentState.getNewState(event)        
        except Exception:
            err = "Error at line " + str(StateMachine.lineCount) + ": invalid \
            event " + eventToStr(event)
            raise Exception(err)

        self.currentState.work(words)

    def getMessages():
        return StateMachine.messages
    
    def getMsgIds():
        return StateMachine.msgIds

class State:
    def __init__(self, name):
        self.eventToNextState = dict()
        self.name = name

    def connect(self, event, nextState):
        self.eventToNextState[event] = nextState

    def work(self, words):
        pass

    def getNewState(self, event):
        return self.eventToNextState[event]

class ErrorState(State):
    def work(self, words):
        err = "Error at line " + str(StateMachine.lineCount) + "Error state reached"
        raise Exception(err)

class BegState(State):
    def work(self, words):
        StateMachine.currentMsg = Msg()
        StateMachine.currentMsg.isFlatBuffer = StateMachine.isFlatMsg
        StateMachine.currentMsg.isSpi = StateMachine.isSpi

class FieldsState(State):
    def work(self, words):
        if(len(words) < 2):
            err = "Error at line " + str(StateMachine.lineCount) + ": less than 2 words"
            raise Exception(err)

        fieldType = None
        
        try:
            fieldType = strToType(words[0])
        except Exception:
            err = "Error at line " + str(StateMachine.lineCount) + ": unknown type: " + words[0]
            raise Exception(err)

        pos = words[1].find('[') 
        name = words[1]
        typeSpec = TypeSpec.PLAIN

        if(pos != -1):
            typeSpec = typeSpec.ARRAY
            name = name[:pos]
        
        pos = words[0].find('*') 

        if(pos != -1):
            typeSpec = typeSpec.PTR
            name = name[:pos]
 
        field = Field(fieldType, name, typeSpec)
        StateMachine.currentMsg.addField(field)

class EndState(State):
    def work(self, words):
        StateMachine.currentMsg.name = words[-1]

class ExternState(State):
    def work(self, words):
        StateMachine.currentMsg.initName = words[-1]
        msgId = StateMachine.currentMsg.initName.replace("INIT_", "")

        if not msgId in StateMachine.msgIdSet:
            err = "Error at line " + str(StateMachine.lineCount) + ": initName=\"" + \
                StateMachine.currentMsg.initName + "\" is not compatible with any msgId name"
            raise Exception(err)
        
        StateMachine.currentMsg.idName = msgId
        StateMachine.messages.append(StateMachine.currentMsg)

class EnumState(State):
    def work(self, words):
       StateMachine.msgIds.append(words[0])

class EnumEndState(State):
    def work(self, words):
        StateMachine.msgIdSet = set(StateMachine.msgIds)

class FlatState(State):
    def work(self, words):
        StateMachine.isFlatMsg = True

class SpiState(State):
    def work(self, words):
        StateMachine.isSpi = True
        StateMachine.isFlatMsg = True


def parse(fileName):
    #print ("Parsing %s ..." % fileName)

    errorState = ErrorState("errorState")
    outState = State("outState")
    begEnumState = State("begEnumState")
    enumState = EnumState("enumState")
    enumEndState = EnumEndState("enumEndState")
    typedefState = State("typedefState")
    begState = BegState("begState")
    fieldState = FieldsState("fieldState")
    endState = EndState("endState")
    externState = ExternState("externState")
    flatState = FlatState("flatState")
    spiState = SpiState("spiState")


    outState.connect(Event.NONE, outState)    
    outState.connect(Event.STR_LITERAL, outState)    
    outState.connect(Event.BRACE_CLOSED, outState)    
    outState.connect(Event.BRACE_OPEN, outState)    
    outState.connect(Event.EXTERN, outState)    
    outState.connect(Event.TYPEDEF, typedefState)
    outState.connect(Event.ENUM, begEnumState)
    outState.connect(Event.START_FLAT_MSG, flatState)
    flatState.connect(Event.NONE, outState)    
    outState.connect(Event.START_SPI_MSG, spiState)
    spiState.connect(Event.NONE, outState)
    begEnumState.connect(Event.STR_LITERAL, enumState)
    enumState.connect(Event.STR_LITERAL, enumState)
    enumState.connect(Event.BRACE_CLOSED, enumEndState)
    enumEndState.connect(Event.NONE, outState)
    typedefState.connect(Event.BRACE_OPEN, begState)
    begState.connect(Event.STR_LITERAL, fieldState)
    fieldState.connect(Event.STR_LITERAL, fieldState)
    fieldState.connect(Event.BRACE_CLOSED, endState)
    endState.connect(Event.NONE, outState)
    endState.connect(Event.EXTERN, externState)
    externState.connect(Event.NONE, outState)

    sm = StateMachine(outState)

    with open(fileName, 'r') as f:
        for line in f:
            sm.process(line) 

    #print ("Parsing %s [DONE]" % fileName)

    return StateMachine.getMessages()


def generateImplFile(messages):
    print ("Genarating message impl file ...")

    with open(outDir + "/generated/messages.cpp", "w") as f:
        f.write("#include \"messages.h\"\n\n\n") 
        
        for msg in messages:
            idName = msg.initName.replace("INIT_", "")
            f.write("const %s %s = {%s, sizeof(%s)};\n" % (msg.name, msg.initName, idName, msg.name))

        f.write("\n\n\nuint16_t getMsgSize(void* msg)\n")
        f.write("{\n    return ((MsgHeader*)msg)->msgLen;\n}")
        f.write("\n\nint32_t getResponseId(int32_t requestId)\n")
        f.write("{\n    switch(requestId)\n")
        f.write("    {\n")
        
        idNames = []
        
        for msg in messages:
            idNames.append(msg.idName)

        idNames.sort()
        err = "Error while genarating  getResponseId function." 
        REQUEST_SUFFIX = "_MSG_REQ"
        RESPONSE_SUFFIX = "_MSG_RSP"
        expectedRespIdName = ""

        for i in range(len(idNames)):
            if not (i % 2):
                if idNames[i].find(REQUEST_SUFFIX) == -1:
                    err +=  " Expected request idName. Instead got " + idNames[i]
                    raise Exception(err)
                expectedRespIdName = idNames[i].replace(REQUEST_SUFFIX, "") + RESPONSE_SUFFIX
                f.write("    case %s:\n" % (idNames[i]))
            else:
                if idNames[i] != expectedRespIdName:
                    err +=  " Expected response " + idNames[i] + ". Instead got " + idNames[i]
                    raise Exception(err)
                f.write("        return %s;\n" % (expectedRespIdName))

        f.write("    default:\n")
        f.write("        break;\n}\n\n")
        f.write("    return -1;\n}\n")

    print ("Genarating message impl file [DONE]")

typeToFlatTypeMap = {
        Type.U8         : "ubyte",
        Type.U16        : "ushort",
        Type.U32        : "uint",
        Type.U64        : "ulong",
        Type.I8         : "byte",
        Type.I16        : "short",
        Type.I32        : "int",
        Type.I64        : "long",
        Type.FLOAT      : "float",
        Type.DOUBLE     : "double",
        Type.BOOL       : "bool",
        Type.MSGHEADER  : "MsgHeader"
        }

def typeToFlatTypeStr(tp):
    return typeToFlatTypeMap[tp]


class FlatSchemaGenerator:
    namespace = "message"
    def __init__(self):
        self.filePrefix = outDir + "/generated/schemas/"
        self.content = ""

    def generate(self):
        pass

    def writeFile(self):
        with open(self.filePrefix + ".fbs", 'w') as f:
            f.write(self.content)

class MsgIdSchemaGenarator(FlatSchemaGenerator):
    def __init__(self, msgIgs):
        self.filePrefix = outDir + "/generated/schemas/msgIds"
        self.msgIgs = msgIgs

    def generate(self):
        print("Generating %s%s ..." % (self.filePrefix, ".fbs"))
        self.content = "namespace " + FlatSchemaGenerator.namespace + ";\n\n" + \
                "enum MsgId : ubyte {\n";

        cnt = 0
        lastId = len(self.msgIgs) - 1

        for msgId in self.msgIgs:
            self.content += "    " + msgId 
            suffix = ",\n" if cnt != lastId else "\n"
            self.content += suffix
            cnt += 1

        self.content += "}" #"\n\nroot_type MsgId;"
        self.writeFile()
    
        print("Generating %s%s [DONE]" % (self.filePrefix, ".fbs"))

class MsgSchemaGenerator(FlatSchemaGenerator):
    def __init__(self, message):
        self.filePrefix = outDir + "/generated/schemas/" + message.name[0].lower() + message.name[1:]
        self.msgName = message.name
        self.fields = message.fields

    def generate(self):
        print("Generating %s%s ..." % (self.filePrefix, ".fbs"))
        self.content = "\nnamespace " + \
                FlatSchemaGenerator.namespace + \
                ";\n\ntable " + self.msgName + " {\n"

        for field in self.fields:
            if field.type == Type.MSGHEADER:
                continue

            typeStr = ""

            if field.typeSpec == TypeSpec.ARRAY:
                typeStr = "[" + typeToFlatTypeStr(field.type) + "]"
            else:
                typeStr = typeToFlatTypeStr(field.type)

            self.content += "    " + field.name + " : " + typeStr + ";\n"

        self.content += "}\n\nroot_type " + self.msgName +";"
        self.writeFile()        
        print("Generating %s%s [DONE]" % (self.filePrefix, ".fbs"))

class MsgMainSchemaGenerator(FlatSchemaGenerator):
    def __init__(self, messages):
        self.filePrefix = outDir + "/generated/schemas/message"
        self.messages = messages

    def generate(self):
        print("Generating %s%s ..." % (self.filePrefix, ".fbs"))
        self.content = "include \"msgHeader.fbs\";\n"
        
        for msg in self.messages:
            fbsFileName = msg.name[0].lower() + msg.name[1:]
            self.content += "include \"" + fbsFileName + ".fbs\";\n"

        self.content += "\nnamespace " + \
                FlatSchemaGenerator.namespace + \
                ";\n\nunion Payload {\n"

        last = len(self.messages)
        cnt = 1

        for msg in self.messages:
            if cnt >= last:
                self.content += "    " + msg.name + "\n"
            else:
                self.content += "    " + msg.name + ",\n"

            cnt += 1

        self.content += "}\n\n"
        self.content += "table Message {\n" +\
                "    header : " + typeToFlatTypeStr(Type.MSGHEADER) + ";\n" +\
                "    payload : Payload;\n}\n\n"

        self.content += "root_type Message;"
        self.writeFile()        
        print("Generating %s%s [DONE]" % (self.filePrefix, ".fbs"))


def createHeader():
    header = Msg()
    header.name = "MsgHeader"
    field = Field(Type.U8, "msgId", TypeSpec.PLAIN)
    header.addField(field)
    field = Field(Type.U8, "msgLen", TypeSpec.PLAIN)
    header.addField(field)
    field = Field(Type.U8, "queueId", TypeSpec.PLAIN)
    header.addField(field)
    field = Field(Type.U8, "slot", TypeSpec.PLAIN)
    header.addField(field)
    
    return header


   

def generateFbsFiles(msgIds, messages):
    schemaGenerators = []
    schemaGenerators.append(MsgIdSchemaGenarator(msgIds))

    for msg in messages:
        schemaGenerators.append(MsgSchemaGenerator(msg))
    
    schemaGenerators.append(MsgSchemaGenerator(createHeader()))
    schemaGenerators.append(MsgMainSchemaGenerator(messages))

    for generator in schemaGenerators:
        generator.generate()

class SerializerGenerator:
    def __init__(self, messages):
        self.messages = messages
        self.hContent = ""
        self.cppContent = ""

    def createHContent(self):
        self.hContent = "#include \"messages.h\"\n" + \
                "#include \"flatbuffers/flatbuffers.h\"\n" + \
                "namespace serialization {\n\n" + \
                "class Serializer\n{\npublic:\n" + \
                "    Serializer();\n" + \
                "    void serialize(const char* buffer, int msgId);\n"

        for msg in self.messages:
            self.hContent += "    void serialize(const " + msg.name + "&);\n"

        self.hContent += "    uint8_t* getBuffer();\n" + \
                "    int getBufferSize();\n"
        self.hContent += "private:\n" + \
                "    flatbuffers::FlatBufferBuilder _builder;\n" + \
                "};\n\n}"

    def createCppContent(self):
        self.cppContent = "#include \"serializer.hpp\"\n"

#        for msg in self.messages:
#            fileName = msg.name[0].lower() + msg.name[1:] + "_generated.h"
#            self.cppContent += "#include \"" + fileName + "\"\n"
        self.cppContent += "#include \"message_generated.h\"\n"

        self.cppContent += "\nnamespace serialization {\n\n" + \
            "Serializer::Serializer() : _builder(4) { }\n\n" + \
            "void Serializer::serialize(const char* buffer, int msgId) {\n" + \
            "    switch(msgId)\n" + \
            "    {\n"

        for msg in self.messages:
            self.cppContent += "    case " + msg.idName + ":\n    {\n" + \
                "        serialize(*reinterpret_cast<const " + msg.name + "*>(buffer));\n" + \
                "        return;\n    }\n"
        
        self.cppContent += "    default:\n" +\
            "        break;\n" + \
            "    }\n\n" + \
            "    throw std::runtime_error(\"Serializer::serialize: Unrecognized msgId:\" + std::to_string(msgId));\n}\n\n"
 

        for msg in self.messages:
            self.cppContent += "void Serializer::serialize(const " + msg.name + "& msg) {\n"
            
            firstArray = True

            for field in msg.fields:
                if field.typeSpec == TypeSpec.ARRAY:
                    self.cppContent += "    " + ("int " if firstArray else "") + \
                    "arrSize = sizeof(msg." + field.name + ")/sizeof(" + \
                    typeStrMap[field.type] + ");\n"  + \
                    "    auto " + field.name + " = _builder.CreateVector(msg." + \
                    field.name + ", arrSize);\n"

                    firstArray = False
                elif field.type == Type.MSGHEADER:
                    self.cppContent += "    auto " + field.name + "= message::Create" + typeStrMap[field.type] + \
                            "(_builder, msg.header.msgId, msg.header.msgLen, msg.header.queueId, msg.header.slot);\n" 

            self.cppContent += "    message::" + msg.name + "Builder payloadBuilder(_builder);\n"

            for field in msg.fields:
                if field.typeSpec == TypeSpec.ARRAY:
                    self.cppContent += "    payloadBuilder.add_" + field.name + "(" + field.name + ");\n"
                elif field.type != Type.MSGHEADER:
                    self.cppContent += "    payloadBuilder.add_" + field.name + "(msg." +  field.name + ");\n"
 
            self.cppContent += "\n    auto payload = payloadBuilder.Finish();\n" + \
                    "    message::MessageBuilder msgBuilder(_builder);\n"

            for field in msg.fields:
                if field.type == Type.MSGHEADER:
                    self.cppContent += "    msgBuilder.add_header(" + field.name + ");\n"

            self.cppContent += "    msgBuilder.add_payload_type(message::Payload_" + msg.name + ");\n" + \
                    "    msgBuilder.add_payload(payload.Union());\n" #TODO: does Union() need to be really called
            self.cppContent += "\n    auto builtMsg = msgBuilder.Finish();\n" + \
                "    _builder.FinishSizePrefixed(builtMsg);\n}\n\n"


        self.cppContent += "uint8_t* Serializer::getBuffer(){\n" + \
                "    return _builder.GetBufferPointer();\n}\n\n"

        self.cppContent += "int Serializer::getBufferSize(){\n" + \
                "    return _builder.GetSize();\n}\n\n}"

    def writeFiles(self):
        with open(outDir + "/generated/serdes/serializer.hpp", 'w') as f:
            f.write(self.hContent)

        with open(outDir + "/generated/serdes/serializer.cpp", 'w') as f:
            f.write(self.cppContent)



    def generate(self):
        self.createHContent()
        self.createCppContent()
        self.writeFiles()

class DeserializerGenerator:
    def __init__(self, messages):
        self.messages = messages
        self.hContent = ""
        self.cppContent = ""

    def createHContent(self):
        self.hContent = "#include \"messages.h\"\n" + \
                "#include \"msg.hpp\"\n" + \
                "#include \"flatbuffers/flatbuffers.h\"\n\n" + \
                "namespace serialization {\n\n" + \
                "class Deserializer\n{\npublic:\n" + \
                "    Deserializer(unsigned char* buffer);\n"

        self.hContent += "    std::unique_ptr<message::Msg> deserialize();\n" + \
                "    uint8_t* getBuffer();\n" + \
                "    int getBufferSize();\n" \
                "    int getMsgId();\n"
        self.hContent += "private:\n"

        for msg in self.messages:
            self.hContent += "    void doDeserialize(" + msg.name + "&);\n"

        self.hContent += "\n    unsigned char*  _buffer;\n" + \
                "};\n\n}"

    def createCppContent(self):
        self.cppContent = "#include \"deserializer.hpp\"\n" + \
                          "#include \"msg.hpp\"\n" + \
                          "#include <stdexcept>\n" + \
                          "#include <algorithm>\n" + \
                          "#include <string>\n" + \
                          "#include \"message_generated.h\"\n"

        self.cppContent += "\nnamespace serialization {\n\n" + \
                "Deserializer::Deserializer(unsigned char* buffer) :\n" + \
                "    _buffer(buffer) { }\n\n" 

        self.cppContent += "std::unique_ptr<message::Msg> Deserializer::deserialize() {\n" + \
            "    switch(getMsgId())\n" + \
            "    {\n"

        for msg in self.messages:
            self.cppContent += "    case " + msg.idName + ":\n    {\n" + \
                "        auto payload = std::make_unique<" + msg.name + ">();\n" + \
                "        doDeserialize(*payload);\n" + \
                "        return std::make_unique<message::DataMsg<" + msg.name + ">>(std::move(payload));\n    }\n"
        
        self.cppContent += "    default:\n" +\
            "        break;\n" + \
            "    }\n\n" + \
            "    throw std::runtime_error(\"Deserializer::deserialize: Unrecognized msgId:\" + std::to_string(getMsgId()));\n}\n\n"
        
        for msg in self.messages:
            self.cppContent += "void Deserializer::doDeserialize(" + msg.name + "& msg) {\n" + \
                    "    auto srcMsg = flatbuffers::GetSizePrefixedRoot<message::Message>(_buffer);\n\n" \
                "    if(srcMsg->payload_type() != message::Payload_" + msg.name + ")\n" \
                "    {\n" \
                "        throw std::runtime_error(\"Deserializer::deserialize: wrong msg type " + msg.name + "\");\n" \
                "    }\n\n" \
                "    auto payload = static_cast<const message::" + msg.name + "*>(srcMsg->payload());\n"

            for field in msg.fields:
                if field.typeSpec == TypeSpec.ARRAY:
                    self.cppContent += "    std::copy(payload->" + field.name + "()->begin(),\n" + \
                                       "              payload->" + field.name + "()->end(),\n" + \
                                       "              msg." + field.name + ");\n"
                elif field.type == Type.MSGHEADER:
                    self.cppContent += "    msg.header.msgId = srcMsg->header()->msgId();\n" \
                                       "    msg.header.msgLen = srcMsg->header()->msgLen();\n" \
                                       "    msg.header.queueId = srcMsg->header()->queueId();\n" \
                                       "    msg.header.slot = srcMsg->header()->slot();\n"
                else:
                    self.cppContent += "    msg." + field.name + " = payload->" +  field.name + "();\n"
            
            self.cppContent += "}\n\n"


        self.cppContent += "uint8_t* Deserializer::getBuffer(){\n" + \
                "    return _buffer;\n}\n\n"

        self.cppContent += "int Deserializer::getMsgId(){\n" + \
                "    auto srcMsg = flatbuffers::GetSizePrefixedRoot<message::Message>(_buffer);\n" \
                "    return srcMsg->header()->msgId();\n}\n\n"

        self.cppContent += "int Deserializer::getBufferSize(){\n" + \
                "    return *(reinterpret_cast<int*>(_buffer));\n}\n\n}"

    def writeFiles(self):
        with open(outDir + "/generated/serdes/deserializer.hpp", 'w') as f:
            f.write(self.hContent)

        with open(outDir + "/generated/serdes/deserializer.cpp", 'w') as f:
            f.write(self.cppContent)



    def generate(self):
        self.createHContent()
        self.createCppContent()
        self.writeFiles()

class SpiDeserializerGenerator:
    def __init__(self, messages):
        self.messages = messages
        self.hContent = ""
        self.cppContent = ""

    def createHContent(self):
        self.hContent = "#include \"messages.h\"\n" + \
                "#include \"msg.hpp\"\n" + \
                "#include <vector>\n\n" + \
                "namespace serialization {\n\n" + \
                "class SpiDeserializer\n{\npublic:\n" + \
                "    SpiDeserializer(const std::vector<char>& buffer);\n"

        self.hContent += "    std::unique_ptr<message::Msg> deserialize();\n"
        self.hContent += "private:\n"


        self.hContent += "\n    std::unique_ptr<message::Msg>  _msg;\n" + \
                "};\n\n}"

    def createCppContent(self):
        self.cppContent = "#include \"spiDeserializer.hpp\"\n" + \
                          "#include <stdexcept>\n" + \
                          "#include <algorithm>\n" + \
                          "#include <string>\n"

        self.cppContent += "\nnamespace serialization {\n\n" + \
                "SpiDeserializer::SpiDeserializer(const std::vector<char>& buffer)\n" + \
                "{\n" + \
                "    auto msgId = *reinterpret_cast<const unsigned char*>(buffer.data());\n\n" +\
                "    switch(msgId)\n" + \
                "    {\n"
    
        for msg in (m for m in self.messages if m.isSpi):
            self.cppContent += "    case " + msg.idName + ":\n    {\n" + \
                "        const auto& payload = *reinterpret_cast<const " + msg.name + "*>(buffer.data());\n" + \
                "        _msg = std::make_unique<message::DataMsg<" + msg.name + ">>(payload);\n" + \
                "        return;\n    }\n"

        self.cppContent += "    default:\n" +\
            "        break;\n" + \
            "    }\n\n" + \
            "    throw std::runtime_error(\"SpiDeserializer::deserialize: Unrecognized msgId:\" + std::to_string(msgId));\n}\n\n"
    
        self.cppContent += "std::unique_ptr<message::Msg> SpiDeserializer::deserialize() {\n" + \
                "    return std::move(_msg);\n" + \
                "}\n\n}"

    def writeFiles(self):
        with open(outDir + "/generated/serdes/spiDeserializer.hpp", 'w') as f:
            f.write(self.hContent)

        with open(outDir + "/generated/serdes/spiDeserializer.cpp", 'w') as f:
            f.write(self.cppContent)



    def generate(self):
        self.createHContent()
        self.createCppContent()
        self.writeFiles()


def generateSerDesFiles(messages):
    serGenerator = SerializerGenerator(messages)
    serGenerator.generate()
    desGenerator = DeserializerGenerator(messages)
    desGenerator.generate()
    spiDesGenerator = SpiDeserializerGenerator(messages)
    spiDesGenerator.generate()

def filterFlatMsgs(messages):
    return [msg for msg in messages if msg.isFlatBuffer]

def printGeneratedSchemas(inPath, outPath):
    generatedDir = outPath + "/generated"
    messages = parse(inPath + "/messages.h")
    flatMessages = filterFlatMsgs(messages)
    
    files = []

    for msg in flatMessages:
        files.append(str(generatedDir + "/schemas/" + msg.name[0].lower() + msg.name[1:] + ".fbs"))
    
    files.append(str(generatedDir + "/schemas/message.fbs"))
    files.append(str(generatedDir + "/schemas/msgHeader.fbs"))
    files.append(str(generatedDir + "/schemas/msgIds.fbs"))
    
    sys.stdout.write(";".join(files))

def main():
    global inDir
    global outDir

    if sys.argv[1] == "--print-schemas":
        inDir = sys.argv[2]
        outDir = sys.argv[3]
        printGeneratedSchemas(inDir, outDir)
        return

    inDir = sys.argv[1]
    outDir = sys.argv[2]
    generatedDir = outDir + "/generated"

    if os.path.isdir(generatedDir):
        shutil.rmtree(generatedDir)
    
    os.makedirs(generatedDir)
    os.makedirs(generatedDir + "/schemas")
    os.makedirs(generatedDir + "/serdes")
    os.makedirs(generatedDir + "/includes")

    messages = parse(inDir + "/messages.h")
    generateImplFile(messages)
    flatMessages = filterFlatMsgs(messages)
    generateFbsFiles(StateMachine.getMsgIds(), flatMessages) 
    generateSerDesFiles(flatMessages)

if __name__ == "__main__":
    main()


